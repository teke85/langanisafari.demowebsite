'use client';

import { useRef, useState } from 'react';
import gsap from 'gsap';
import { useGSAP } from '@gsap/react';
import Nav from './Nav';
import ExpandedMenu from './ExpandedMenu';
import Logo from './Logo';
import Link from 'next/link';
import UserMenu from './UserMenu';

export default function Header() {
  const container = useRef();
  const tl = useRef();
  const [isOverLayOPen, setIsOverLayOpen] = useState(false);

  const { contextSafe } = useGSAP(() => {
    tl.current = gsap
      .timeline()
      .to(container.current, {
        opacity: 1,
        stagger: 0.8,
        duration: 2.5,
        y: 40,
        delay: 16,
        ease: 'power2.inOut', // Use easing for smoother animation
      })
      .to(['#nav-item', '#nav-item', '#nav-item'], {
        opacity: 1,
        delay: 0.5,
        scrub: 1,
        duration: 4,
        stagger: 0.3,
      });
  }, [{ scope: container }]);
 
  return (
    <>
      {/* Overlay menu */}
      <div className="fixed overlay left-0 top-[100vh] z-10 cursor-pointer overlay-menu h-[100vh] w-[100%] bg-teal-700">
        <button className="absolute top-10 left-10">&#x2715; CLOSE</button>
        <div className="flex h-full items-center justify-center">
          <div className="flex items-center justify-center  w-12/12 md:w-6/12">
            1
          </div>
          <div className="flex flex-col items-center justify-center w-12/12 md:w-6/12">
            <div className="menu-info-col">
              <Link href="/">X</Link>
              <Link href="/">Instagram</Link>
              <Link href="/">Facebook</Link>
            </div>
          </div>
        </div>
      </div>
      {/* <ExpandedMenu ref={menu} /> */}
      <header
        ref={container}
        className="flex opacity-0 mt-[-40px] w-full items-center border-b-[0.5px]"
      >
        <div className="container-fluid w-full px-4">
          <div className="flex items-center justify-between">
            <div
              id="nav-item"
              className="toggleBtn-wrapper flex justify-between opacity-0 w-5/12"
            >
              <button className="cursor-pointer">MENU</button>
              <Nav />
            </div>
            <div
              id="nav-item"
              className="logo-wrapper opacity-0 flex justify-start md:justify-center w-4/12 md:w-4/12"
            >
              <Link href="/">
                <Logo />
              </Link>
            </div>
            <div
              id="nav-item"
              className="btnWrapper opacity-0 flex gap-8 justify-center w-5/12"
            >
              <button
                id="button"
                className="flex text-[11px] items-center justify-center text-black uppercase p-5 bg-[#ECECEC] h-10 rounded-full"
              >
                Book your stay
              </button>
              <button
                id="button"
                className="flex text-[11px] items-center justify-center text-gray uppercase p-5 transition duration-200 hover:text-white/55 bg-black/40 hover:bg-black/70 h-10 rounded-full"
              >
                Make a Reservation
              </button>
            </div>
          </div>
        </div>
      </header>
    </>
  );
}
