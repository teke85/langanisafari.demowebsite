'use client';

import React, { useState, useEffect, useRef } from 'react';
import Image from 'next/image';

const images = [
  { id: 1, src: '/assets/photos/nature_1.jpg', category: 'Nature' },
  { id: 2, src: '/assets/photos/food_16.jpg', category: 'Food' },
  { id: 3, src: '/assets/photos/nature_2.jpg', category: 'Nature' },
  { id: 4, src: '/assets/photos/Food_17.jpg', category: 'Food' },
  { id: 5, src: '/assets/photos/food_2.jpg', category: 'Food' },
  { id: 6, src: '/assets/photos/nature_3.jpg', category: 'Nature' },
  { id: 7, src: '/assets/photos/food_6.jpg', category: 'Food' },
  { id: 8, src: '/assets/photos/room_1.jpg', category: 'Rooms' },
  { id: 9, src: '/assets/photos/room_2.jpg', category: 'Rooms' },
  { id: 10, src: '/assets/photos/room_3.jpg', category: 'Rooms' },
];

const categories = ['All', 'Nature', 'Interior', 'Food','Rooms'];

const InfiniteScroll = () => {
  const [selectedCategory, setSelectedCategory] = useState('All');
  const scrollRef = useRef(null);

  const filteredImages =
    selectedCategory === 'All'
      ? images
      : images.filter((image) => image.category === selectedCategory);

  useEffect(() => {
    const handleHover = () => {
      scrollRef.current.style.animationPlayState = 'paused';
    };

    const handleLeave = () => {
      scrollRef.current.style.animationPlayState = 'running';
    };

    const scrollElement = scrollRef.current;
    scrollElement.addEventListener('mouseenter', handleHover);
    scrollElement.addEventListener('mouseleave', handleLeave);

    return () => {
      scrollElement.removeEventListener('mouseenter', handleHover);
      scrollElement.removeEventListener('mouseleave', handleLeave);
    };
  }, []);

  return (
    <div className="relative w-full overflow-hidden">
      <div className="flex justify-center mb-4">
        {categories.map((category) => (
          <button
            key={category}
            className={`mx-2 px-2 md:px-4 text-[15px] py-2 rounded-md font-montserrat ${
              selectedCategory === category
                ? 'bg-[#272F3C] text-white'
                : 'bg-gray-200 text-gray-700'
            }`}
            onClick={() => setSelectedCategory(category)}
          >
            {category}
          </button>
        ))}
      </div>
      <div className="relative w-full flex justify-center items-center overflow-hidden">
        {/* Blur effect at the start */}
        <div className="absolute top-0 left-0 w-16 h-full bg-gradient-to-r from-gray-100 to-transparent pointer-events-none z-10"></div>

        <div
          ref={scrollRef}
          className="scroll-container relative overflow-hidden w-full max-w-5xl"
        >
          <div className="scroll-content flex space-x-8 animate-scroll">
            {filteredImages.map((image, index) => (
              <div key={index} className="flex-none w-64 h-64">
                <Image
                  src={image.src}
                  alt={`Image ${image.id}`}
                  width={900}
                  height={900}
                  quality={100}
                  className="object-cover w-full h-full"
                />
              </div>
            ))}
            {filteredImages.map((image, index) => (
              <div key={`${index}-copy`} className="flex-none w-64 h-64">
                <Image
                  src={image.src}
                  alt={`Image ${image.id}`}
                  width={256}
                  height={256}
                  className="object-cover w-full h-full"
                />
              </div>
            ))}
          </div>
        </div>

        {/* Blur effect at the end */}
        <div className="absolute top-0 right-0 w-16 h-full bg-gradient-to-l from-gray-100 to-transparent pointer-events-none z-10"></div>
      </div>
    </div>
  );
};

export default InfiniteScroll;
