'use client';

import Image from 'next/image';
import React, { useState } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import Select from 'react-select';

const BookingForm = () => {
  const [checkInDate, setCheckInDate] = useState(null);
  const [checkOutDate, setCheckOutDate] = useState(null);
  const [adults, setAdults] = useState(1);
  const [children, setChildren] = useState(0);

  const options = [
    { value: 0, label: '0' },
    { value: 1, label: '1' },
    { value: 2, label: '2' },
    { value: 3, label: '3' },
    { value: 4, label: '4' },
  ];

  const handleSearch = () => {
    // Handle the search action here
    console.log('Searching with:', {
      checkInDate,
      checkOutDate,
      adults,
      children,
    });
  };

  return (
    <div className="flex md:px-10 flex-col md:flex-row w-[90%] md:rounded-full bg-black justify-between items-start md:items-center gap-4 p-4">
      <div className="text-white font-montserrat">
        <label className="block mb-2 font-semibold text-[17px] font-ibarra">
          Check-in *
        </label>
        <DatePicker
          selected={checkInDate}
          onChange={(date) => setCheckInDate(date)}
          placeholderText="Check-in Date"
          className="w-full p-2 border-b-2 font-lora bg-black text-white"
        />
      </div>
      <div className="text-white">
        <label className="block mb-2 font-semibold text-[17px] font-ibarra">
          Check-out *
        </label>
        <DatePicker
          selected={checkOutDate}
          onChange={(date) => setCheckOutDate(date)}
          placeholderText="Check-out Date"
          className="w-full p-2 border-b-2 font-lora bg-black text-white"
        />
      </div>
      <div className="text-white">
        <label className="block mb-2 font-lora">Adults</label>
        <select
          value={adults}
          onChange={(e) => setAdults(parseInt(e.target.value))}
          className="w-full p-2 border-b-2 font-lora bg-black text-white"
        >
          {[0, 1, 2, 3, 4].map((option) => (
            <option key={option} value={option}>
              {option}
            </option>
          ))}
        </select>
      </div>
      <div className="text-white">
        <label className="block mb-2 font-lora">Children</label>
        <select
          value={children}
          onChange={(e) => setChildren(parseInt(e.target.value))}
          className="w-full p-2 border-b-2 font-lora bg-black text-white"
        >
          {[0, 1, 2, 3, 4].map((option) => (
            <option key={option} value={option}>
              {option}
            </option>
          ))}
        </select>
      </div>
      <button
        onClick={handleSearch}
        className="text-black font-semibold bg-white p-2 w-1/4 md:w-[150px] font-montserrat text-[11px] md:text-[15px] hover:bg-gray-600 hover:text-softwhite"
      >
        SEARCH
      </button>
    </div>
  );
};

export default BookingForm;
