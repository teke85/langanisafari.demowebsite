'use client';

import { ReactLenis, useLenis } from 'lenis/react';
import { useEffect } from 'react';

function SmoothScroll({ children }) {
  const lenis = useLenis(() => {
    console.log('Scroll event triggered');
  });

  useEffect(() => {
    if (lenis) {
      console.log('Lenis initialized:', lenis);
      lenis.start();
    } else {
      console.log('Lenis is not initialized.');
    }
    return () => {
      if (lenis) {
        console.log('Stopping Lenis.');
        lenis.stop();
      }
    };
  }, [lenis]);

  return (
    <ReactLenis
      root
      options={{
        duration: 8,
        smoothTouch: true,
        lerp: 0.2,
        gestureDirection: 'vertical',
        smooth: true,
        touchMultiplier: 2,
      }}
    >
      {children}
    </ReactLenis>
  );
}

export default SmoothScroll;
