import Image from 'next/image';
import React from 'react';

const Logo = () => {
  return (
    <div className="flex justify-center items-center p-4 md:w-[10vw] w-[20vw]">
      <Image
        className="relative rounded-full"
        src="/assets/logos/color.jpg"
        alt="langani safari logo"
        width={100}
        height={100}
        priority
      />
    </div>
  );
};

export default Logo;
