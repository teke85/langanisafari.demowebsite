'use client';
import react, { useRef } from 'react';
import Image from 'next/image';
import gsap from 'gsap';
import { useGSAP } from '@gsap/react';
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger';

const GsapSquare = () => {
  const imageRef = useRef();
  const container = useRef();
  gsap.registerPlugin(ScrollTrigger);

  // const onClickGood = () => {
  //   gsap.to('.good', {
  //     y: 50,
  //     duration: 1,
  //     ease: 'power2.out', // Use easing for smoother animation
  //     scale: 1,
  //   });
  // };

  useGSAP(() => {
    const el = imageRef.current;
    const t1 = gsap.timeline();
    t1.to('.text', {
      opacity: 1,
      delay: 2.6,
      y: '-=100', // Slide in from left
      duration: 0.8,
      ease: 'easeInOut', // Use easing for smoother animation
      scrollTrigger: {
        trigger: '.text',
        markers: false,
      },
      scrub: 0.6,
      stagger: 0.1,
    });
    t1.to(imageRef.current, {
      duration: 1,
      opacity: 1,
      ease: 'easeInOut',
    });
  }, [{ scope: container }]);

  return (
    <div
      className="flex flex-col justify-center items-center gap-4"
      ref={container}
    >
      <div className="flex flex-col md:flex-row p-5 justify-between items-end">
        <p className="text opacity-0 w-full md:w-1/2 text-xl md:text-3xl">
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Corporis
          consequuntur magni soluta possimus, perferendis est quibusdam
          inventore laudantium adipisci eveniet odio?
        </p>
        <Image
          ref={imageRef}
          className="relative opacity-0"
          src="/assets/photos/wedding_reception.jpg"
          alt="picture of a swimming pool"
          width={500}
          height={1000}
          priority
        />
      </div>
    </div>
  );
};

export default GsapSquare;
