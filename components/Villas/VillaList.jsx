// 'use client';

// import React from 'react';
// import Image from 'next/image';
// import Link from 'next/link';
// import { FaStar, FaStarHalf } from 'react-icons/fa';

// const VillaList = ({ villas }) => {
//   return (
//     <section className="py-16 w-full h-full">
//       <div className="text-black container mx-auto grid grid-cols-1 md:grid-cols-2 gap-6">
//         {villas.data.length === 0 ? (
//           <div className="text-center flex flex-col bg-white w-full p-4 rounded-lg shadow-md">
//             <h2 className="text-lg font-bold font-ibarra">
//               No Villas Available
//             </h2>
//             <p className="text-gray-600 font-ibarra">
//               We couldn&apos;t find any villas at the moment.
//             </p>
//           </div>
//         ) : (
//           villas.data.map((villa) => {
//             const imgURL = `${villa.attributes.image.data?.attributes.url}`;

//             return (
//               <div
//                 key={villa.id}
//                 className="relative flex flex-col h-full group bg-white shadow-md overflow-hidden"
//               >
//                 <Link href={`/villas/${villa.id}`} passHref>
//                   <div className="relative w-full h-72 mb-6">
//                     <Image
//                       src={imgURL}
//                       fill
//                       priority
//                       alt={villa.attributes.name || 'Villa Image'}
//                       className="object-cover w-full h-full"
//                     />
//                     <div className="absolute inset-0 flex items-center justify-center opacity-0 group-hover:opacity-100 transition-opacity duration-500 bg-black bg-opacity-60">
//                       <span className="text-white px-4 py-2 bg-black rounded shadow-lg hover:bg-black-700">
//                         View More Details
//                       </span>
//                     </div>
//                   </div>
//                 </Link>
//                 <div className="flex flex-col gap-3 p-4">
//                   <div className="flex flex-col md:flex-row items-start justify-between">
//                     <div className="font-ibarra">
//                       Capacity - {villa.attributes.capacity} person
//                     </div>
//                     <div className="flex gap-1 text-gray-500">
//                       <FaStar />
//                       <FaStar />
//                       <FaStar />
//                       <FaStar />
//                       <FaStarHalf />
//                     </div>
//                   </div>
//                   <Link href={`/villas/${villa.id}`} passHref>
//                     <h3 className="font-ibarra text-lg font-semibold">
//                       {villa.attributes.name}
//                     </h3>
//                   </Link>
//                   <p className="font-medium font-ibarra">
//                     $ {villa.attributes.pricePerNight}{' '}
//                     <span className="font-ibarra">/ night</span>
//                   </p>
//                 </div>
//               </div>
//             );
//           })
//         )}
//       </div>
//     </section>
//   );
// };

// export default VillaList;
