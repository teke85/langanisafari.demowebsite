import Link from "next/link";
import Image from "next/image";

const Nav = () => {
  return (
    <div className="flex flex-row items-center text-white-500">
      <nav className="z-40 text-sm">
        <ul className="nav hidden md:flex gap-4">
          {["Villas", "Dining", "Relax", "Discover"].map((item, index) => (
            <li className="nav-item group" key={index}>
              <Link
                className="relative font-lora text-[16px] inline-block after:content-[''] after:absolute after:left-0 after:right-0 after:bottom-0 after:h-[2px] after:bg-white after:scale-x-0 after:origin-left after:transition-transform after:duration-300 group-hover:after:scale-x-100"
                href={`/`} // href={`/${item.toLowerCase()}`}
              >
                {item}
              </Link>
            </li>
          ))}
        </ul>
      </nav>
    </div>
  );
};

export default Nav;
