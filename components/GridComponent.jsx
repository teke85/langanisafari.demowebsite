import React, { useEffect, useRef } from 'react';
import gsap from 'gsap';
import { useGSAP } from '@gsap/react';
import { ScrollTrigger } from 'gsap/ScrollTrigger';
import Image from 'next/image';

gsap.registerPlugin(ScrollTrigger);

const Dining = [
  {
    image: '/assets/photos/room_1.jpg',
    title: 'Beautiful Beach House',
    description:
      'Enjoy a luxurious stay at our beautiful beach house with stunning ocean views.',
    link: '#',
  },
  {
    image: '/assets/photos/room_2.jpg',
    title: 'Mountain Retreat',
    description:
      'Escape to our peaceful mountain retreat, surrounded by nature.',
    link: '#',
  },
  {
    image: '/assets/photos/room_3.jpg',
    title: 'City Apartment',
    description: 'Experience the urban lifestyle in our modern city apartment.',
    link: '#',
  },
  {
    image: '/assets/photos/room_3.jpg',
    title: 'City Apartment',
    description: 'Experience the urban lifestyle in our modern city apartment.',
    link: '#',
  },
  {
    image: '/assets/photos/room_3.jpg',
    title: 'City Apartment',
    description: 'Experience the urban lifestyle in our modern city apartment.',
    link: '#',
  },
  {
    image: '/assets/photos/room_3.jpg',
    title: 'City Apartment',
    description: 'Experience the urban lifestyle in our modern city apartment.',
    link: '#',
  },
  // Add more places as needed
];

const GridComponent = () => {
  const imagesRef = useRef([]);

  useGSAP(() => {
    imagesRef.current.forEach((image, index) => {
      gsap.fromTo(
        image,
        { y: 200 },
        {
          y: 0,
          stagger: 1,
          scrollTrigger: {
            trigger: image,
            start: 'top 90%',
            end: 'bottom 20%',
            scrub: true,
          },
        }
      );
    });
  }, []);

  return (
    <section>
      <div className="grid grid-cols-1 md:grid-cols-3 gap-4 p-4">
        {Dining.map((place, index) => (
          <div
            key={index}
            className={`relative cursor-pointer transition-transform duration-300 ${
              index === 2 ? 'hover:scale-110' : ''
            }`}
            ref={(el) => (imagesRef.current[index] = el)}
          >
            <div className="shadow-lg">
              <Image
                src={place.image}
                alt={place.title}
                layout="responsive"
                width={600}
                height={400}
                className="object-cover w-full h-full rounded-lg"
              />
            </div>
          </div>
        ))}
      </div>
    </section>
  );
};

export default GridComponent;
