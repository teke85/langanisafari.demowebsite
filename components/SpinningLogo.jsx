'use client';

import { useRef } from 'react';
import gsap from 'gsap';
import { useGSAP } from '@gsap/react';
import ReactCurvedText from 'react-curved-text';

const SpinningLogo = () => {
  const container = useRef();
  const circle = useRef();

  useGSAP(
    () => {
      gsap.to(circle.current, {
        rotation: 360,
        duration: 1,
        repeat: 1,
        repeatDelay: 1,
        yoyo: true,
      });
    },
    { scope: container }
  );

  return (
    <div className="container">
      <div
        ref={circle}
        className="flex spin h-20 w-20 rounded-full border"
      >
        <ReactCurvedText
          width={370}
          height={300}
          cx={190}
          cy={120}
          rx={100}
          ry={100}
          startOffset={100}
          text="Liseli Lodge"
          reversed={true}
          textProps={{ style: { fontSize: 14 } }}
          textPathProps={{ fill: '#ffffff' }}
        />
      </div>
    </div>
  );
};

export default SpinningLogo;
