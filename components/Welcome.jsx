'use client'

import React from 'react'

const Welcome = () => {
  return (
    <section className="welcome h-screen w-full">
      <div className="grid grid-cols-1 md:grid-cols-2"></div>
    </section>
  )
}

export default Welcome
