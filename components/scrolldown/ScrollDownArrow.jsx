import React from 'react';
import { FiArrowDown } from 'react-icons/fi';

const ScrollDownArrow = () => {
  return (
    <div className="flex flex-col items-center absolute bottom-8 left-1/2 transform -translate-x-1/2">
      <FiArrowDown className="animate-bounce text-white w-5 h-5 mb-2" />
      <span className="font-montserrat text-[11px] uppercase font-semibold"
        >Scroll Down</span>
      {/* <FiArrowDown
        className="animate-bounce text-white w-8 h-8 mb-2"
        style={{ animationDelay: '0.2s' }}
      />
      <FiArrowDown
        className="animate-bounce text-white w-8 h-8"
        style={{ animationDelay: '0.4s' }}
      /> */}
    </div>
  );
};

export default ScrollDownArrow;
