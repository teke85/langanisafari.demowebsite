'use client';

import gsap from 'gsap';
import { useGSAP } from '@gsap/react';
import { useRef, useEffect } from 'react';
import Image from 'next/image';
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger';

const animals = [
  {
    image: '/assets/photos/nature_18.jpg',
    title: 'Buffalo',
    description:
      'Enjoy a luxurious stay at our beautiful beach house with stunning ocean views.',
    link: '#',
  },
  {
    image: '/assets/photos/nature_12.jpg',
    title: 'Lion',
    description:
      'Escape to our peaceful mountain retreat, surrounded by nature.',
    link: '#',
  },
  {
    image: '/assets/photos/nature_13.jpg',
    title: '',
    description:
      'Escape to our peaceful mountain retreat, surrounded by nature.',
    link: '#',
  },
  {
    image: '/assets/photos/nature_14.png',
    title: '',
    description:
      'Escape to our peaceful mountain retreat, surrounded by nature.',
    link: '#',
  },
  {
    image: '/assets/photos/nature_15.jpg',
    title: '',
    description:
      'Escape to our peaceful mountain retreat, surrounded by nature.',
    link: '#',
  },
  // Add more places as needed
];

function ScrollSection() {
  const sectionRef = useRef(null);
  const triggerRef = useRef(null);

  gsap.registerPlugin(ScrollTrigger);

  useGSAP(() => {
    gsap.fromTo(
      sectionRef.current,
      {
        translateX: 0,
      },
      {
        translateX: '-300vw',
        ease: 'power1.inOut',
        duration: 1,
        scale: 1,
        scrollTrigger: {
          trigger: triggerRef.current,
          start: 'top top',
          end: '2000 top',
          scrub: 0.6,
          pin: true,
        },
      }
    );
  }, []);

  return (
    <section className="scroll-section-outer mt-20 bg-white min-h-[100vh] overflow-hidden">
      <div ref={triggerRef}>
        <div
          ref={sectionRef}
          className="scroll-section-inner h-[100vh] w-[400vw] flex items-center bg-red relative"
        >
          {animals.map((animal, index) => (
            <div
              key={index}
              className="scroll-section h-[100vh] w-[100vw] flex justify-center items-start"
            >
              <div className="bg-white border-4 h-[70vh] w-full md:w-[50vw] relative shadow-2xl">
                <Image
                  className="p-4"
                  src={animal.image}
                  alt={animal.title}
                  fill
                  priority={true}
                  objectFit="cover"
                />
              </div>
            </div>
          ))}
          {/* <div className="scroll-section h-[100vh] w-[100vw] flex justify-center items-center">
            <div className="bg-white h-[50vh] w-[50vw] relative">
              <span className="absolute top-0 left-0 z-10 p-3">
                Swimming pool Area
              </span>
              <Image
                className="relative dark:drop-shadow-[0_0_0.3rem_#ffffff70] dark:invert"
                src="/assets/photos/nature_12"
                alt="picture of a swimming pool"
                fill
                priority
              />
            </div>
          </div>
          <div className="scroll-section h-[100vh] w-[100vw] flex justify-center items-center">
            <div className="bg-white h-[50vh] w-[50vw] relative">
              <span className="absolute top-0 left-0 z-10 p-3">
                Swimming pool Area
              </span>
              <Image
                className="relative dark:drop-shadow-[0_0_0.3rem_#ffffff70] dark:invert"
                src="/assets/photos/nature_13"
                alt="picture of a swimming pool"
                fill
                priority
              />
            </div>
          </div>
          <div className="scroll-section h-[100vh] w-[100vw] flex justify-center items-center">
            <div className="bg-white h-[50vh] w-[50vw] relative">
              <span className="absolute top-0 left-0 z-10 p-3">
                Swimming pool Area
              </span>
              <Image
                className="relative dark:drop-shadow-[0_0_0.3rem_#ffffff70] dark:invert"
                src="/assets/photos/nature_14"
                alt="picture of a swimming pool"
                fill
                priority
              />
            </div>
          </div> */}
        </div>
      </div>
    </section>
  );
}

export default ScrollSection;
