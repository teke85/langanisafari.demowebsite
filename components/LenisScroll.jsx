import { useRef, useEffect } from 'react';
import Lenis from '@studio-freight/lenis';

const LenisScroll = ({ children }) => {
  const lenisRef = useRef(null);
  const rafHandleRef = useRef(null);

  useEffect(() => {
    // Initialize Lenis on the first render
    if (!lenisRef.current) {
      lenisRef.current = new Lenis({
        duration: 2.8,
        easing: (t) => Math.min(1, 1.001 - Math.pow(2, -10 * t)),
        smoothWheel: true,
        smoothTouch: true,
      });

      const raf = (time) => {
        lenisRef.current.raf(time);
        rafHandleRef.current = requestAnimationFrame(raf);
      };

      rafHandleRef.current = requestAnimationFrame(raf);
    }

    // Clean up on component unmount
    return () => {
      if (lenisRef.current) {
        lenisRef.current.destroy();
        lenisRef.current = null;
      }
      if (rafHandleRef.current) {
        cancelAnimationFrame(rafHandleRef.current);
        rafHandleRef.current = null;
      }
    };
  }, []);

  return <div>{children}</div>;
};

export default LenisScroll;
