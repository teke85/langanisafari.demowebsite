// components/RevealTextFramer.js
import { motion } from 'framer-motion';
import { useEffect, useState } from 'react';

const RevealTextFramer = ({ text }) => {
  const [isInView, setIsInView] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      const element = document.getElementById('reveal-text');
      if (element) {
        const rect = element.getBoundingClientRect();
        if (
          rect.top >= 0 &&
          rect.bottom <=
            (window.innerHeight || document.documentElement.clientHeight)
        ) {
          setIsInView(true);
        } else {
          setIsInView(false);
        }
      }
    };

    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  }, []);

  return (
    <section className="flex items-center justify-center h-screen">
      <motion.div
        id="reveal-text"
        className="overflow-hidden"
        initial={{ y: '100%', opacity: 0 }}
        animate={{ y: isInView ? '0%' : '100%' }}
        transition={{ duration: 0.8, ease: 'easeOut' }}
      >
        <div className="text-4xl font-bold">{text}</div>
      </motion.div>
    </section>
  );
};

export default RevealTextFramer;
