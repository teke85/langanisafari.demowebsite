'use client';

import { useState, useEffect } from 'react';
import { Button } from '@/components/ui/button';
import { Calendar } from '@/components/ui/calendar';
import { cn } from '@/lib/utils';
import { format, isPast, differenceInCalendarDays } from 'date-fns';
import { Calendar as CalendarIcon } from 'lucide-react';
import {
  Table,
  TableBody,
  TableCaption,
  TableCell,
  TableFooter,
  TableHead,
  TableHeader,
  TableRow,
} from '@/components/ui/table';
import {
  Popover,
  PopoverContent,
  PopoverTrigger,
} from '@/components/ui/popover';
import toast, { Toaster } from 'react-hot-toast';
import { FaCreditCard, FaPaypal, FaMobileAlt } from 'react-icons/fa';

const Reservation = ({ villa }) => {
  const [checkInDate, setCheckInDate] = useState(null);
  const [checkOutDate, setCheckOutDate] = useState(null);
  const [guests, setGuests] = useState(1);
  const [firstname, setFirstname] = useState('');
  const [lastname, setLastname] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const [totalAmount, setTotalAmount] = useState(0);
  const [selectedPaymentMethod, setSelectedPaymentMethod] = useState('');

  useEffect(() => {
    if (checkInDate && checkOutDate) {
      const nights = differenceInCalendarDays(checkOutDate, checkInDate);
      setTotalAmount(nights * villa.data.attributes.pricePerNight);
    }
  }, [checkInDate, checkOutDate, villa.data.attributes.pricePerNight]);

  const handleGuestsChange = (e) => {
    setGuests(Number(e.target.value));
  };

  const handlePaymentMethodChange = (method) => {
    setSelectedPaymentMethod(method);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (
      !checkInDate ||
      !checkOutDate ||
      !firstname ||
      !lastname ||
      !email ||
      !phone
    ) {
      toast.error('Please fill in all the fields required.');
      return;
    }

    const reservationData = {
      data: {
        villa: villa.id,
        checkIn: checkInDate,
        checkOut: checkOutDate,
        guests,
        firstname,
        lastname,
        email,
        phone,
        totalAmount,
        paymentMethod: selectedPaymentMethod,
      },
    };

    try {
      const response = await fetch(
        `${process.env.API_STRAPI_URL}/reservations`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${process.env.STRAPI_API_TOKEN}`,
          },
          body: JSON.stringify(reservationData),
        }
      );

      if (!response.ok) {
        throw new Error('Failed to submit reservation');
      }

      toast.success('Reservation submitted successfully!');
      // Clear form
      setCheckInDate(null);
      setCheckOutDate(null);
      setGuests(1);
      setFirstname('');
      setLastname('');
      setEmail('');
      setPhone('');
      setTotalAmount(0);
      setSelectedPaymentMethod('');
    } catch (error) {
      toast.error('Failed to submit reservation. Please try again.');
    }
  };

  return (
    <div className="flex flex-col items-center gap-5 w-full max-w-md mx-auto p-5 border rounded-lg shadow-lg bg-white">
      <Toaster />
      <h2 className="text-2xl font-bold mb-4">Reservation & Booking</h2>
      <form onSubmit={handleSubmit} className="w-full flex flex-col gap-3">
        {/* Check-in */}
        <Popover>
          <PopoverTrigger asChild>
            <Button
              variant={'outline'}
              className={cn(
                'w-full justify-start text-left font-ibarra',
                !checkInDate && 'text-muted-foreground'
              )}
            >
              <CalendarIcon className="mr-2 h-4 w-4" />
              {checkInDate ? (
                format(checkInDate, 'PPP')
              ) : (
                <span className="font-ibarra">CHECK IN DATE</span>
              )}
            </Button>
          </PopoverTrigger>
          <PopoverContent className="w-auto p-0">
            <Calendar
              mode="single"
              selected={checkInDate}
              onSelect={setCheckInDate}
              initialFocus
              disabled={isPast}
            />
          </PopoverContent>
        </Popover>
        {/* Check-out */}
        <Popover>
          <PopoverTrigger asChild>
            <Button
              variant={'outline'}
              className={cn(
                'w-full justify-start text-left font-ibarra',
                !checkOutDate && 'text-muted-foreground'
              )}
            >
              <CalendarIcon className="mr-2 h-4 w-4" />
              {checkOutDate ? (
                format(checkOutDate, 'PPP')
              ) : (
                <span className="font-ibarra">CHECK OUT DATE</span>
              )}
            </Button>
          </PopoverTrigger>
          <PopoverContent className="w-auto p-0">
            <Calendar
              mode="single"
              selected={checkOutDate}
              onSelect={setCheckOutDate}
              initialFocus
              disabled={isPast}
            />
          </PopoverContent>
        </Popover>
        {/* Guests */}
        <div className="w-full mt-4">
          <label
            htmlFor="guests"
            className="block text-sm font-medium font-poppins text-gray-700"
          >
            Number of Guests
          </label>
          <input
            id="guests"
            type="number"
            min="1"
            max={villa.data.attributes.capacity}
            value={guests}
            onChange={handleGuestsChange}
            className="mt-1 block w-full font-ibarra border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
          />
        </div>
        {/* Personal Information */}
        <div className="w-full mt-4">
          <label
            htmlFor="firstname"
            className="block text-sm font-medium font-poppins text-gray-700"
          >
            First Name
          </label>
          <input
            id="firstname"
            type="text"
            value={firstname}
            onChange={(e) => setFirstname(e.target.value)}
            className="mt-1 block w-full border font-ibarra border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
          />
        </div>
        <div className="w-full mt-4">
          <label
            htmlFor="lastname"
            className="block text-sm font-medium font-poppins text-gray-700"
          >
            Last Name
          </label>
          <input
            id="lastname"
            type="text"
            value={lastname}
            onChange={(e) => setLastname(e.target.value)}
            className="mt-1 block w-full font-ibarra border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
          />
        </div>
        <div className="w-full mt-4">
          <label
            htmlFor="email"
            className="block text-sm font-medium font-poppins text-gray-700"
          >
            Email Address
          </label>
          <input
            id="email"
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            className="mt-1 block w-full border font-ibarra border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
          />
        </div>
        <div className="w-full mt-4">
          <label
            htmlFor="phone"
            className="block text-sm font-medium text-gray-700 font-poppins"
          >
            Phone Number
          </label>
          <input
            id="phone"
            type="tel"
            value={phone}
            onChange={(e) => setPhone(e.target.value)}
            className="mt-1 block w-full border font-ibarra border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm"
          />
        </div>
        {/* Reservation Summary */}
        <div className="w-full flex flex-col gap-1 bg-gray-100 p-4 rounded-lg shadow-inner mt-4">
          <h3 className="text-sm md:text-lg font-bold tracking-normal mb-2 font-poppins">
            Reservation Summary
          </h3>
          <Table>
            <TableBody>
              <TableRow>
                <TableCell>
                  <strong className="font-montserrat">Villa:</strong>
                </TableCell>
                <TableCell>{villa.data.attributes.name}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  <strong className="font-montserrat">Check-in:</strong>
                </TableCell>
                <TableCell>
                  {checkInDate ? format(checkInDate, 'PPP') : 'N/A'}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  <strong className="font-montserrat">Check-out:</strong>
                </TableCell>
                <TableCell>
                  {checkOutDate ? format(checkOutDate, 'PPP') : 'N/A'}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  <strong className="font-montserrat">Guests:</strong>
                </TableCell>
                <TableCell>{guests}</TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  <strong className="font-montserrat">Total Amount:</strong>
                </TableCell>
                <TableCell>${totalAmount.toFixed(2)}</TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </div>
        {/* Reservation button */}
        <Button type="submit" className="w-full mt-6 font-montserrat">
          Submit Reservation
        </Button>
        {/* Payment Options */}
        <div className="w-full flex flex-col items-center mt-6">
          <h3 className="text-lg font-bold mb-2 font-poppins">
            Payment Method
          </h3>
          <div className="flex flex-col md:flex-row  gap-1 justify-center w-full mb-4">
            <Button
              variant={
                selectedPaymentMethod === 'credit-card' ? 'primary' : 'outline'
              }
              onClick={() => handlePaymentMethodChange('credit-card')}
              className="flex items-center font-ibarra gap-2"
            >
              <FaCreditCard className="w-3 h-3 text-gray-400" />
              Credit Card
            </Button>
            <Button
              variant={
                selectedPaymentMethod === 'mobile-money' ? 'primary' : 'outline'
              }
              onClick={() => handlePaymentMethodChange('mobile-money')}
              className="flex items-center font-ibarra gap-2"
            >
              <FaMobileAlt className="w-3 h-3 text-red-500" />
              Mobile Money
            </Button>
            <Button
              variant={
                selectedPaymentMethod === 'paypal' ? 'primary' : 'outline'
              }
              onClick={() => handlePaymentMethodChange('paypal')}
              className="flex items-center gap-2 font-ibarra"
            >
              <FaPaypal className="w-3 h-3 text-blue-500" />
              PayPal
            </Button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default Reservation;
