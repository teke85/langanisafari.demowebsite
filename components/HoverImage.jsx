import { useState, useEffect, useRef } from 'react';
import { gsap } from 'gsap';
import Image from 'next/image';
import { useGSAP } from '@gsap/react';

const images = [
  {
    image: '/assets/photos/food_1.jpg',
  },
  {
    image: '/assets/photos/restaurant.jpg',
  },
  {
    image: '/assets/photos/sunset.jpg',
  },
  {
    image: '/assets/photos/guided_safari.jpg',
  },
  {
    image: '/assets/photos/room_4.jpg',
  },
];

const HoverImageComponent = () => {
  const [currentImage, setCurrentImage] = useState(images[0]);
  const tl = useRef(null);

  useGSAP(() => {
    // Initialize GSAP timeline for text animation
    tl.current = gsap.timeline({ paused: true });

    tl.current.to('.text-link', {
      y: -10,
      opacity: 1,
      duration: 0.3,
      ease: 'power3.out',
    });

    tl.current.to('.text-link', {
      y: 0,
      opacity: 0,
      duration: 0.3,
      ease: 'power3.out',
    });

    // Cleanup function
    return () => {
      tl.current.kill(); // Kill GSAP timeline on unmount
    };
  }, []);

  const handleMouseEnter = (index) => {
    gsap.to('.image-container img', {
      opacity: 0,
      duration: 0.2,
      onComplete: () => {
        setCurrentImage(images[index]);
        gsap.to('.image-container img', {
          opacity: 1,
          duration: 0.2,
        });
      },
    });

    tl.current.restart(); // Restart GSAP timeline for text animation
  };

  return (
    <section className="flex flex-col justify-center items-center w-full h-screen md:h-[50vh] bg-white">
      <div className="text-center mb-10">
        <h2 className="text-4xl text-black font-aboreto">
          Explore Our Experiences
        </h2>
      </div>
      <div className="flex items-center justify-between w-full p-10">
        <div className="w-1/2 italic font-ibarra space-y-4">
          {[
            'Food & Beverage',
            'Bar & Restaurant',
            'Sunset Views',
            'Exciting Activities',
            'Luxury Rooms',
          ].map((text, index) => (
            <div
              key={index}
              className="text-2xl cursor-pointer text-gray-700 hover:text-gray-500 transition duration-300 relative"
              onMouseEnter={() => handleMouseEnter(index)}
            >
              <span className="text-link absolute bottom-0 left-0 w-full h-0 bg-gray-500 opacity-0"></span>
              {text}
            </div>
          ))}
        </div>
        <div className="w-1 bg-black border-2"></div>
        <div className="w-full md:w-2/4 p-10 h-[50vh] relative image-container">
          <Image
            src={currentImage.image}
            alt="Current"
            className="w-full h-full p-10 transition-opacity duration-500"
            objectFit="cover"
            layout="fill"
          />
        </div>
      </div>
    </section>
  );
};

export default HoverImageComponent;
