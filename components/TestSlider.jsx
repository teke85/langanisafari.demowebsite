'use client';
import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Image from 'next/image';

const TestSlider = () => {
  const settings = {
    dots: true,
    infinite: true,
    className: 'center',
    speed: 800,
    centerPadding: '60px',
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 8000,
    pauseOnHover: true,
    appendDots: (dots) => (
      <div
        style={{
          borderRadius: '1px',
          padding: '50px',
        }}
      >
        <ul style={{ margin: '0px' }}> {dots} </ul>
      </div>
    ),
  };

  const sliderData = [
    {
      id: 1,
      image: '/assets/photos/room_1.jpg',
      name: 'Client One',
      message:
        '"Absolutely stunning! Our weekend getaway at the villa was nothing short of magical!".',
    },
    {
      id: 2,
      image: '/assets/photos/room_2.jpg',
      name: 'Client Two',
      message: 'This is a message from Client Two.',
    },
    {
      id: 3,
      image: '/assets/photos/room_3.jpg',
      name: 'Client Three',
      message: 'This is a message from Client Three.',
    },
    {
      id: 4,
      image: '/assets/photos/room_3.jpg',
      name: 'Client Four',
      message: 'This is a message from Client Four.',
    },
  ];

  return (
    <section className="h-full">
      <div className="flex flex-col overflow-hidden w-full mx-auto">
        <Slider {...settings}>
          {sliderData.map((slide) => (
            <div
              key={slide.id}
              className="flex flex-col items-center cursor-pointer justify-center w-full h-80 p-4"
            >
              <div className="flex flex-col items-center justify-center h-full w-full">
                <div className="w-16 h-16 overflow-hidden rounded-full">
                  <Image
                    src={slide.image}
                    alt={slide.name}
                    width={150}
                    height={150}
                    className="object-cover w-full h-full"
                  />
                </div>
                <h3 className="text-black mt-4 font- text-center text-xl">
                  {slide.name}
                </h3>
                <p className="text-black font-montserrat text-sm mt-2 text-center">
                  {slide.message}
                </p>
              </div>
            </div>
          ))}
        </Slider>
      </div>
    </section>
  );
};

export default TestSlider;
