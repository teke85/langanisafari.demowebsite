import React from 'react';

const Heading = ({ text }) => {
  return (
    <section className="w-full">
      <div className="container">
        <h3 className="heading font-montserrat text-start text-[#B0B0B0] font-semibold font-sm">
          {text}
        </h3>
      </div>
    </section>
  );
};

export default Heading;
