'use client'
import {
  useScroll,
  motion,
  useSpring,
  useTransform,
  useMotionValueEvent,
} from 'framer-motion';
import React, { useState, useRef, useEffect } from 'react';

const Smooth = ({ children }) => {
  const [isLoading, setIsLoading] = useState(true);
  const contentRef = useRef(null);
  const [contentHeight, setContentHeight] = useState(0);
  const [windowHeight, setWindowHeight] = useState(0);

  useEffect(() => {
    const handleResize = () => {
      if (contentRef.current != null) {
        setContentHeight(contentRef.current.scrollHeight);
      }
      setWindowHeight(windowHeight);
    };

    handleResize();

    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [contentRef]);

  // Intercept normal scroll behaviour

  const { scrollYProgress } = useScroll();
  const smoothProgress = useSpring(scrollYProgress, {
    mass: 0.1,
    stiffness: 100,
    damping: 20,
    restDelta: 0.001,
  });

  useMotionValueEvent(smoothProgress, 'change', (latest) => {
    if (latest === 0) {
      setIsLoading(false);
    }
  });

  const y = useTransform(smoothProgress, (value) => {
    return value * -(contentHeight - windowHeight);
  });

  return (
    <>
      <div style={{ height: contentHeight }} />
      <motion.div
        ref={contentRef}
        style={{ y: isLoading ? 0 : y, opacity: isLoading ? 0 : 1 }}
        className="w-screen flex flex-col fixed top-0 transition-opacity duration-200 ease-in-out"
      >
        {children}
      </motion.div>
    </>
  );
};

export default Smooth;
