"use client";

import Link from "next/link";
import React, { useState } from "react";

const UserMenu = () => {
  return (
    <div>
      <span>
        <button className="uppercase">Menu</button>
      </span>
    </div>
  );
};

export default UserMenu;

// 'use client';

// const UserMenu = () => {
//   <div className="">
//     <span className="user-menu text-red-500">Menu</span>
//   </div>;
// };

// export default UserMenu;
