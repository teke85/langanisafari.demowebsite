import React from 'react';

const GoogleMapEmbed = () => {
  return (
    <div className="container mx-auto px-4">
      <div className="flex items-center justify-center">
        <span className="text-lg font-semibold">LOCATION</span>
      </div>
      <div className="w-full h-50 mt-5">
        <iframe
          width="100%"
          height="100%"
          frameBorder="0"
          className="border-0"
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3153.837961521317!2d144.9556516159118!3d-37.81821757975144!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad65d43f1f6e441%3A0x7d84f3076b9b437!2sFlinders%20St%20Station!5e0!3m2!1sen!2sau!4v1605275653676!5m2!1sen!2sau"
          allowFullScreen=""
          aria-hidden="false"
          tabIndex="0"
        ></iframe>
      </div>
    </div>
  );
};

export default GoogleMapEmbed;
