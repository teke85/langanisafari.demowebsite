import Link from 'next/link';
import Header from './header/GsapHeader';
import Logo from './Logo';
import { FaInstagram, FaFacebook, FaTwitter } from 'react-icons/fa';
import GoogleMapEmbed from './GoogleMapEmbed';

function Footer() {
  return (
    <section className="relative w-full h-[500px]">
      <div className="container py-10 flex items-center flex-col mx-auto h-full">
        <Logo />
        <div className="grid grid-col-1 md:grid-cols-4 wrapper h-full bg-gray-400 w-full">
          {/* <div className="flex flex-col items-center justify-center gap-10">
            <GoogleMapEmbed />
          </div> */}
          <div className="flex flex-col items-center justify-center gap-10">
            <span>Book Your Stay</span>
            <span>Call us +260-760-609896 or</span>
            <span>Email:info@langanisafariresort.com</span>
          </div>
          <div className="flex flex-col items-center justify-center gap-10">
            <span>RESERVATIONS</span>
            <span>Call us +260-760-609896 or</span>
            <span>Email:info@langanisafariresort.com</span>
          </div>
          <div className="flex flex-col items-center justify-center gap-10">
            <span className="font-lora">FOLLOW US</span>
            <div className="flex space-x-10">
              <Link href="https://www.instagram.com/langanisafari">
                <FaInstagram className="text-2xl text-[#ECECEC]" />
              </Link>
              <Link href="/https://web.facebook.com/profile.php?id=61554289924623">
                <FaFacebook className="text-2xl text-[#ECECEC]" />
              </Link>
              <Link href="/">
                <FaTwitter className="text-2xl text-[#ECECEC]" />
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Footer;
