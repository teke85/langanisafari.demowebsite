'use client';

import { useRef, useState } from 'react';
import gsap from 'gsap';
import { useGSAP } from '@gsap/react';
import Nav from './Nav';
import ExpandedMenu from './ExpandedMenu';
import Logo from './Logo'
import UserMenu from './UserMenu';

export default function Header() {
  const [menuClicked, setMenuClicked] = useState(false);
  const header = useRef();
  const menu = useRef();

  function handleMenuClicked() {
    setMenuClicked(!menuClicked);
  }

  useGSAP(() => {
    const tl = gsap.timeline();
    tl.to(header.current, {
      opacity: 1,
      stagger: 0.8,
      duration: 3,
      y: 40,
      delay: 14,
      ease: 'power2.inOut', // Use easing for smoother animation
    });
  }, [{ scope: header }]);

  return (
    <>
      <ExpandedMenu toggle={menuClicked} />
      <header
        ref={header}
        className="flex opacity-0 mt-[-40px] w-full items-center z-[9] h-10"
      >
        <div className="container-fluid w-full p-4">
          <div className="flex w-full justify-between">
            <div className="flex">
              <Nav />
              logo
            </div>
            <div>
              <span>
                <button
                  ref={menu}
                  onClick={handleMenuClicked}
                  className="cursor-pointer uppercase z-50"
                >
                  Menu
                </button>
              </span>
            </div>
          </div>
        </div>
      </header>
    </>
  );
};

