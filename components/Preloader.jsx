import { useEffect, useRef } from 'react';
import gsap from 'gsap';
import Logo from './Logo';

const Preloader = ({ onComplete }) => {
  const comp = useRef(null);

  useEffect(() => {
    console.log('Preloader mounted');
    const t1 = gsap.timeline({
      onComplete: () => {
        console.log('Preloader animation complete');
        localStorage.setItem('preloaderShown', 'true');
        onComplete();
      },
    });

    t1.set('#intro-slider', { opacity: 1, yPercent: 0 }) // Ensure the initial position
      .from('#intro-slider', {
        yPercent: '-100',
        duration: 1.2,
        delay: 0.5,
      })
      .to('.logo', {
        opacity: 1,
        duration: 1.1,
      })
      .from(['#title-1', '#title-2', '#title-3'], {
        opacity: 0,
        y: '+=30',
        stagger: 0.5, // Stagger the appearance
        duration: 0.8, // Duration for each title
      })
      .to(['#title-1', '#title-2', '#title-3'], {
        opacity: 0,
        y: '-=30',
        stagger: 0.5, // Stagger the disappearance
        duration: 0.8, // Duration for each title
      })
      .to('#intro-slider', {
        yPercent: '-100',
        delay: 0.5,
        duration: 1.4,
      });
  }, [comp]);

  return (
    <div ref={comp} className="flex relative h-[140vh] md:h-[100vh] flex-col">
      <div
        id="intro-slider"
        className="flex flex-col justify-center items-center w-full h-screen absolute top-0 left-0 z-10 p-10 bg-gray-500 tracking-tight text-black text-3xl gap-4 font-playfair_display transform translate-y-full opacity-0"
      >
        <div className="logo opacity-0">
          <Logo />
        </div>
        <div className="texts flex flex-col gap-10 text-white text-center font-ibarra font-light italic">
          <h5 id="title-1" className="opacity-0">
            Indulge in Unparalleled Luxury
          </h5>
          <h5 id="title-2" className="opacity-0">
            Where Comfort exceeds expectations
          </h5>
          <h5 id="title-3" className="opacity-0">
            Experience Authentic Hospitality
          </h5>
        </div>
      </div>
    </div>
  );
};

export default Preloader;
