'use client';

import React, { useRef } from 'react';
import gsap from 'gsap';
import { useGSAP } from '@gsap/react';

function ExpandedMenu() {
  const overlayContainer = useRef();
  const menuRef = useRef();

  useGSAP(() => {
    const tl = gsap.timeline();
    tl.to(menuRef.current, {
      top: 0,
      duration: 2,
      ease: 'power2.inOut', // Use easing for smoother animation
    });
  }, [overlayContainer]);

  return (
    <div ref={overlayContainer}>
      <div
        ref={menuRef}
        className="fixed left-0 top-0 z-10 cursor-pointer overlay-menu h-[100vh] w-[100%] bg-teal-700"
      >
        <span className="absolute top-10 left-10">
          &#x2715; Close
        </span>
        <div className="flex h-full items-center justify-center">
          <div className="flex items-center justify-center  w-12/12 md:w-6/12">
            1
          </div>
          <div className="flex items-center justify-center w-12/12 md:w-6/12">
            2
          </div>
        </div>
      </div>
    </div>
  );
}

export default ExpandedMenu;

// import React, { useRef } from 'react';
// import gsap from 'gsap';
// import { useGSAP } from '@gsap/react';

// function ExpandedMenu(props) {
//   const menu = useRef();
//   const toggle = props.toggle;
//   const tl = gsap.timeline();

//   useGSAP(() => {
//     gsap.timeline();
//     tl.to(menu.current, {
//       top: '0',
//       duration: 0.7,
//       opacity: 1,
//       ease: 'easeInOut',
//     });
//   }, [{ scope: menu }]);

//   return (
//     <div
//       ref={menu}
//       className="fixed top-[-100vh] z-10 overlay-menu h-[100vh] w-[100%] bg-teal-700"
//     >
//       Menu
//     </div>
//   );
// }

// export default ExpandedMenu;
