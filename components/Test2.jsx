'use client';
import { useRef } from 'react';
import gsap from 'gsap';
import { useGSAP } from '@gsap/react';
import Link from 'next/link';

const Test2 = () => {
  const container = useRef();
  const textRef = useRef();
  const linkRef = useRef();
  // store the timeline in a ref.
  const tl = useRef();

  const { contextSafe } = useGSAP(
    () => {
      tl.current = gsap
        .timeline()
        .to('.box', {
          yPercent: '-=100',
          duration: 0.9,
          scrub: true,
          ease: 'power2.out',
        })
        .to(textRef.current, {
          opacity: 0,
          y: 20,
        });
    },
    { scope: container }
  ); // <-- scope for selector text (optional)

  const onClickToggle = contextSafe(() => {
    tl.current.reversed(!tl.current.reversed());
    // tl.current.set('#link', { opacity: 0 });
    // if (!tl.current.reversed()) {
    //   // Animation to fade text upwards when box opens
    //   // gsap.from('#link', {
    //   //   opacity: 0,
    //   //   y: '+=20', // Move the text upwards
    //   //   ease: 'power1.inOut',
    //   // });
    gsap.to('.box', {
      opacity: 1,
      duration: 10,
    });
    //   // .to('#link', {
    //   //   opacity: 0,
    //   //   delay: 4,
    //   //   y: '-=20', // Move the text downwards
    //   //   duration: 0.2,
    //   //   ease: 'power2.inOut',
    //   // });
    // }
  });

  return (
    <div
      ref={container}
      className="flex min-h-screen w-full justify-between items-center"
    >
      <div className="box flex flex-col">
        <div className="p-10 bg-white flex items-center justify-between text-black w-[90vw] h-screen overflow-hidden">
          <ul
            ref={linkRef}
            className="nav-container text-black link flex flex-col space-y-[50px] border p-4 h-[50%] justify-center items-start"
          >
            <Link href="/">
              <li id="link">Home</li>
            </Link>
            <Link href="/">
              <li id="link">Liseli Lodge</li>
            </Link>
            <Link href="/">
              <li id="link">Make a Booking</li>
            </Link>
          </ul>
          <p ref={textRef} className="sidemenutext w-1/6 p-4">
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore
            nisi exercitationem delectus a provident sed, excepturi, eius
            inventore autem, error quidem doloribus voluptatem non quod ex.
            Maiores nihil culpa error.
          </p>
          <button
            onClick={onClickToggle}
            className="good bg-slate-300 rounded-2xl w-36 p-2 z-20 text-white"
          >
            Menu2
          </button>
        </div>
      </div>
      <button
        onClick={onClickToggle}
        className="good bg-slate-300 rounded-2xl w-36 p-2 z-20 text-white"
      >
        Menu3
      </button>
    </div>
  );
};

export default Test2;
