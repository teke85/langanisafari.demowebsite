'use client';

import Image from 'next/image';
import React, { useRef } from 'react';
import gsap from 'gsap';
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger';
import { useGSAP } from '@gsap/react';

const AnimatedImages = () => {
  const imagebox = useRef();

  useGSAP(() => {
    gsap.registerPlugin(ScrollTrigger);
    const tl = gsap.timeline({
      scrollTrigger: {
        trigger: imagebox.current,
        toggleActions: 'restart none none reset',
        start: 'top 50%',
        end: 'bottom 40%',
        scrub: true, // smooth animation when scrolling
        // markers: {
        //   startColor: 'white',
        //   endColor: 'white',
        // },
      },
    });

    tl.to(imagebox.current, {
      ease: 'power1.inOut',
      opacity: 1,
      xPercent: '-=50',
      duration: 3,
      height: '100px',
    });
  }, []);

  return (
    <section className="flex container items-center justify-end h-screen w-full">
      <div ref={imagebox} className="imagebox opacity-0">
        <Image
          className="relative image"
          src="/assets/photos/wedding_reception.jpg"
          alt="picture of a swimming pool"
          width={500}
          height={250}
          priority
        />
      </div>
    </section>
  );
};

export default AnimatedImages;
