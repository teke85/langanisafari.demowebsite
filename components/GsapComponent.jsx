'use client';

import React from 'react';
import { useRef, useState } from 'react';
import gsap from 'gsap';
import { useGSAP } from '@gsap/react';

const GsapComponent = () => {
  const tl = useRef();
  const [isOpen, setIsOpen] = useState(false);
  const container = useRef();
  const circle = useRef();

  const { contextSafe } = useGSAP(
    () => {
      tl.current = gsap.timeline();
    },
    { scope: container }
  );

  const onClickGood = contextSafe(() => {
    gsap.to('.good', {
      opacity: 1,
      y: '+=100',
      stagger: 0.2,
      rotation: '+=180',
    });
  });

  return (
    <div
      ref={container}
      className="flex justify-between h-screen items-center container p-10"
    >
      <div
        ref={circle}
        className="flex good opacity-0 items-center justify-center rounded-full bg-yellow-500 w-[100px] h-[100px]"
      >
        Circle
      </div>
      <div className="flex good items-center justify-center box bg-teal-500 w-[100px] h-[100px]">
        Box
      </div>
      <button
        onClick={onClickGood}
        className="bg-white text-black font-medium p-4 rounded-2xl"
      >
        Click Me
      </button>
    </div>
  );
};

export default GsapComponent;
