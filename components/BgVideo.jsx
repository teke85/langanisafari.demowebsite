'use client';

const BgVideo = () => {
  return (
    <div className="relative h-screen container mx-auto rounded-xl overflow-hidden">
      <video className="w-full h-full object-cover" loop muted autoPlay>
        <source src="assets/video/video.mp4" />
      </video>
    </div>
  );
};

export default BgVideo;
