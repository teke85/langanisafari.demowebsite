'use client';
import react, { useRef } from 'react';
import Image from 'next/image';
import gsap from 'gsap';
import { useGSAP } from '@gsap/react';
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger';

const Square = () => {
  const textRef = useRef();
  const containerSec = useRef();
  gsap.registerPlugin(ScrollTrigger);

  // const onClickGood = () => {
  //   gsap.to('.good', {
  //     y: 50,
  //     duration: 1,
  //     ease: 'power2.out', // Use easing for smoother animation
  //     scale: 1,
  //   });
  // };

  useGSAP(() => {
    gsap.to(textRef.current, {
      opacity: 1,
      delay: 0.6,
      y: '-=100', // Slide in from left if open, from right if closed
      duration: 1.8,
      ease: 'power2.out', // Use easing for smoother animation
      scrollTrigger: {
        trigger: '.square',
        markers: false,
      },
      scrub: 0.6,
      stagger: 0.1,
    });
  }, [{ scope: containerSec }]);

  return (
    <div
      className="flex flex-col justify-center items-center gap-4"
      ref={containerSec}
    >
      <div className="square flex flex-col md:flex-row p-5 justify-between items-end">
        <Image
          className="relative image"
          src="/assets/photos/wedding_reception.jpg"
          alt="picture of a swimming pool"
          width="500"
          height="250"
          priority
        />
        <p
          ref={textRef}
          className="opacity-0 w-full md:w-1/2 text-xl md:text-3xl"
        >
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Corporis
          consequuntur magni soluta possimus, perferendis est quibusdam
          inventore laudantium adipisci eveniet odio?
        </p>
      </div>
    </div>
  );
};

export default Square;
