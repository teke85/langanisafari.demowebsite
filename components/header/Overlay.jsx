'use client';

import { useEffect, useRef } from 'react';
import gsap from 'gsap';
import Link from 'next/link';
import './overlaymenu.css';
import { useGSAP } from '@gsap/react';
import { FaInstagram, FaFacebook, FaTwitter } from 'react-icons/fa';

const menuLinks = [
  { path: '/', label: 'Home' },
  { path: '/stay', label: 'Stay' },
  { path: '/dining', label: 'Dining' },
  { path: '/about', label: 'Relax' },
  { path: '/discover', label: 'Discover' },
];

const InformationLinks = [
  { path: '/', label: 'Rates & Offers' },
  { path: '/', label: 'Management Services' },
  { path: '/', label: 'Contact Us' },
  { path: '/', label: 'Galleries' },
];

export default function OverlayMenu({ isMenuOpen, toggleMenu }) {
  const container = useRef();
  const tl = useRef(gsap.timeline({ paused: true }));

  useGSAP(() => {
    tl.current
      .set('.menu-link-item-holder', { y: 75 })
      .to('.menu-overlay', {
        duration: 1.35,
        clipPath: 'polygon(0% 0%, 100% 0%, 100% 100%, 0% 100%)',
        ease: 'power3.inOut',
      })
      .to('.menu-link-item-holder', {
        opacity: 1,
        y: 0,
        duration: 0.7,
        ease: 'power2.inOut',
        delay: 0.1,
        stagger: 0.01,
      });
  });

  useEffect(() => {
    if (isMenuOpen) {
      tl.current.play();
    } else {
      tl.current.reverse();
    }
  }, [isMenuOpen]);

  return (
    <div
      ref={container}
      className={`fixed menu-overlay z-50 left-0 top-0 flex h-[100vh] w-[100%] ${
        isMenuOpen ? 'open' : ''
      }`}
    >
      <button
        onClick={toggleMenu}
        className="absolute flex items-center gap-2 font-montserrat text-sm top-10 left-10"
      >
        <div className="flex items-center justify-center p-3 border rounded-full w-6 h-6 ">
          &#x2715;
        </div>
        CLOSE
      </button>
      <div className="flex items-center h-full w-full md:px-40 md:py-40">
        <div className="grid grid-col-1 w-7/12 md:grid-cols-2">
          <div className="flex flex-col px-20 md:px-0 gap-4 menu-links">
            <span className="text-[14px] opacity-0 menu-link-item-holder font-semibold font-montserrat">
              NAVIGATION
            </span>
            {menuLinks.map((link, index) => (
              <div className="menu-link-item" key={index}>
                <div
                  className="menu-link-item-holder flex opacity-0 relative"
                  onClick={toggleMenu}
                >
                  <Link
                    href={link.path}
                    className="border-b-2 text-4xl font-aboreto menu-link"
                  >
                    {link.label}
                  </Link>
                </div>
              </div>
            ))}
          </div>
        </div>
        <div className="p-5 hidden md:flex gap-10 w-full justify-between ">
          <div className="flex flex-col gap-3 w-6/12 text-3xl">
            <h3 className="opacity-0 menu-link-item-holder font-aboreto">
              Information
            </h3>
            {InformationLinks.map((link, index) => (
              <div className="opacity-0 menu-link-item-holder" key={index}>
                <Link
                  href={link.path}
                  className="flex font-aboreto flex-col gap-5 text-xl"
                >
                  {link.label}
                </Link>
              </div>
            ))}
          </div>
          <div className="flex flex-col opacity-0 menu-link-item-holder gap-3 w-12/12 text-3xl">
            <h4 className="font-aboreto">Contact</h4>
            <p className="text-xl font-aboreto lowercase">
              Call us +260-760-609896 or
              <br /> Email:info@langanisafariresort.com
            </p>
            <div className="flex gap-5 socials">
              <Link href="https://www.instagram.com/langanisafari">
                <FaInstagram className="text-2xl text-[#ECECEC]" />
              </Link>
              <Link href="/https://web.facebook.com/profile.php?id=61554289924623">
                <FaFacebook className="text-2xl text-[#ECECEC]" />
              </Link>
              <Link href="/">
                <FaTwitter className="text-2xl text-[#ECECEC]" />
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
