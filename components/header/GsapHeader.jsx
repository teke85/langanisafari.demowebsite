"use client";

import { useRef, useEffect, useState } from "react";
import gsap from "gsap";
import { useGSAP } from "@gsap/react";
import Nav from "../Nav";
import ExpandedMenu from "../ExpandedMenu";
import Logo from "../Logo";
import Link from "next/link";
import OverlayMenu from "./Overlay";
import UserMenu from "../UserMenu";
import SVGMenuIcon from "../SVGMenuIcon";

// const menuLinks = [
//   { path: "/", label: "Home" },
//   { path: "/villas", label: "stay" },
//   { path: "/dining", label: "Dining" },
//   { path: "/about", label: "Relax" },
//   { path: "/discover", label: "Discover" },
// ];

export default function Header() {
  const header = useRef();
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [stickyMenu, setStickyMenu] = useState(false);
  const lastScrollY = useRef(0);

  const toggleMenu = () => {
    setIsMenuOpen(!isMenuOpen);
  };

  useEffect(() => {
    const handleScroll = () => {
      const currentScrollY = window.scrollY;

      if (currentScrollY < lastScrollY.current && currentScrollY > 150) {
        // Scrolling up and past a certain point (150px)
        setStickyMenu(true);
      } else {
        // Scrolling down or not past the threshold
        setStickyMenu(false);
      }

      lastScrollY.current = currentScrollY;
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  useGSAP(() => {
    gsap
      .timeline()
      .to(header.current, {
        opacity: 1,
        stagger: 0.8,
        duration: 2.2,
        y: 40,
        delay: 12,
        ease: "power2.inOut", // Use easing for smoother animation
      })
      .to(["#nav-item", "#nav-item", "#nav-item"], {
        opacity: 1,
        delay: 0.5,
        scrub: 1,
        duration: 6,
      });
  }, [{ scope: header }]);

  return (
    <section>
      <OverlayMenu isMenuOpen={isMenuOpen} toggleMenu={toggleMenu} />
      <header
        ref={header}
        className={`flex opacity-0 mt-[-40px] w-full items-center border-b-[0.1px] z-20 transition-all duration-700 ease-in-out ${
          stickyMenu
            ? "bg-white border-b-0 fixed shadow-xl top-0 left-0 text-gray-500"
            : ""
        }`}
      >
        <div className="container-fluid w-full px-4">
          <div className="flex flex-row-reverse md:flex-row items-center justify-between">
            <div
              id="nav-item"
              className="flex justify-between opacity-0 w-5/12"
            >
              <button
                onClick={toggleMenu}
                className={`flex gap-3 items-center cursor-pointer text-softwhite text-[12px] pr-10 font-montserrat border-r-2 ${stickyMenu
            ? 'text-gray-800'
            : ''
        }`}
              >
                <SVGMenuIcon />
                MENU
              </button>
              <Nav />
            </div>
            <div
              id="nav-item"
              className="opacity-0 flex justify-center md:justify-center w-4/12 md:w-4/12"
            >
              <Link href="/">
                <Logo />
              </Link>
            </div>
            <div
              id="nav-item"
              className="hidden opacity-0 md:flex gap-8 justify-center w-5/12"
            >
              <button
                id="button"
                className="hidden md:flex font-medium text-[16px] border font-montserrat items-center justify-center text-black p-5 hover:bg-white/90 bg-white h-10 rounded-full"
              >
                <Link className="font-lora text-gray-500" href="/villas">
                  Book your stay
                </Link>
              </button>
              <button
                id="button"
                className="hidden md:flex bg-white font-medium text-[16px] font-montserrat items-center justify-center text-gray p-5 transition duration-200 border hover:text-black bg-transparent hover:bg-white  h-10 rounded-full"
              >
                <span className="font-lora text-gray-800">
                  Make a Reservation
                </span>
              </button>
            </div>
          </div>
        </div>
      </header>
    </section>
  );
}
