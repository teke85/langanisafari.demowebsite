// components/RevealTextGSAP.js
import { useEffect, useRef } from 'react';
import gsap from 'gsap';
import { ScrollTrigger } from 'gsap/ScrollTrigger';
import { useGSAP } from '@gsap/react';

gsap.registerPlugin(ScrollTrigger);

const RevealTextGSAP = ({ text }) => {
  const textContainerRef = useRef(null);
  const textRef = useRef(null);
  const tl = useRef(gsap.timeline({ paused: true }));

  useGSAP(() => {
    tl.current
      .set(textRef.current, { y: '100%', opacity: 0 })
      .to(textContainerRef.current, {
        duration: 1,
        clipPath: 'polygon(0% 0%, 100% 0%, 100% 100%, 0% 100%)',
        ease: 'power2.inOut',
      })
      .to(textRef.current, {
        y: '0%',
        opacity: 1,
        duration: 0.7,
        ease: 'power2.inOut',
        delay: 0.1,
      });
  }, []);

  useEffect(() => {
    ScrollTrigger.create({
      trigger: textContainerRef.current,
      start: 'top 50%',
      end: 'top 50%',
      onEnter: () => tl.current.play(),
      onLeaveBack: () => tl.current.reverse(),
    });
  }, []);

  return (
    <section className="flex h-[50vh] bg-black justify-center items-center">
      <div ref={textContainerRef} className="overflow-hidden clip-path-start">
        <div
          ref={textRef}
          className="text-2xl text-center font-aboreto font-bold"
        >
          {text}
        </div>
      </div>
    </section>
  );
};

export default RevealTextGSAP;
