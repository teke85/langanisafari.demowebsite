import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Image from 'next/image';
import Link from 'next/link';
import { useState, useRef, useEffect } from 'react';
import { gsap } from 'gsap';

const animals = [
  {
    image: '/assets/photos/guided_safari.jpg',
    title: 'Guided Safari Tours',
    description:
      'Join our expert guides on thrilling safari tours, exploring the rich wildlife and breathtaking landscapes of the African wilderness.',
    link: '#',
  },
  {
    image: '/assets/photos/winetasting.jpg',
    title: 'Wine Tasting',
    description:
      'Savor a selection of fine wines in an exclusive tasting session, guided by our knowledgeable sommeliers.',
    link: '#',
  },
  {
    image: '/assets/photos/fishing.jpg',
    title: 'Fishing in the wilderness',
    description:
      'Experience the thrill of fishing in pristine waters, perfect for both novice and experienced anglers.',
    link: '#',
  },
  {
    image: '/assets/photos/arts.jpg',
    title: 'Arts & Crafts',
    description:
      'Unleash your creativity in our arts and crafts resin workshop, perfect for creating unique souvenirs.',
    link: '#',
  },
  {
    image: '/assets/photos/private_collection.jpg',
    title: 'Langani Private Collection',
    description:
      'Stay in our exclusive private villas, offering unparalleled comfort and privacy amidst stunning natural surroundings.',
    link: '#',
  },
];

const Carousel = () => {
  const [currentSlide, setCurrentSlide] = useState(0);
  const cardRefs = useRef([]);

  useEffect(() => {
    cardRefs.current.forEach((ref, index) => {
      ref.addEventListener('mouseenter', () => handleMouseEnter(index));
      ref.addEventListener('mouseleave', () => handleMouseLeave(index));

      return () => {
        ref.removeEventListener('mouseenter', () => handleMouseEnter(index));
        ref.removeEventListener('mouseleave', () => handleMouseLeave(index));
      };
    });
  }, []);

  const handleMouseEnter = (index) => {
    const elements = cardRefs.current[index].querySelectorAll('.animate');
    gsap.to(elements, {
      y: 0,
      opacity: 1,
      stagger: 0.2,
      duration: 0.5,
    });
  };

  const handleMouseLeave = (index) => {
    const elements = cardRefs.current[index].querySelectorAll('.animate');
    gsap.to(elements, {
      y: 20,
      opacity: 0,
      stagger: 0.2,
      duration: 0.5,
    });
  };

  const settings = {
    dots: true,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 6000,
    pauseOnHover: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    afterChange: (current) => setCurrentSlide(current),
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  const totalSlides = animals.length;

  const addToRefs = (el) => {
    if (el && !cardRefs.current.includes(el)) {
      cardRefs.current.push(el);
    }
  };

  return (
    <section className="flex items-center justify-center h-full bg-white">
      <div className="flex flex-col gap-10 container mx-auto py-8">
        <h2 className="heading font-montserrat text-start text-black/70 font-semibold font-sm mt-20 md:mt-0">
          Explore Our Unique Activities
        </h2>
        <h3 className="font-aboreto text-black text-3xl text-center mb-10 md:text-4xl w-full uppercase">
          Discover Unforgettable Experiences at Langani Safari.
        </h3>
        <Slider {...settings}>
          {animals.map((animal, index) => (
            <div key={index} className="p-4" ref={addToRefs}>
              <div className="wrapper bg-white overflow-hidden shadow-md transition-transform duration-500 transform hover:scale-105">
                <div className="relative w-full h-[85vh] md:h-[85vh]">
                  <Image
                    src={animal.image}
                    alt={animal.title}
                    layout="fill"
                    objectFit="cover"
                    priority={true}
                    quality={100}
                  />
                  <div className="absolute p-10 inset-0 flex flex-col gap-5 justify-center items-center bg-black bg-opacity-50 opacity-0 hover:opacity-100 transition-opacity duration-500">
                    <h3 className="text-xl font-ibarra text-white text-center font-semibold mb-2 animate">
                      {animal.title}
                    </h3>
                    <p className="text-white font-montserrat text-center mb-4 animate">
                      {animal.description}
                    </p>
                    <button
                      id="button"
                      className="hidden md:flex font-medium text-[13px] border font-montserrat items-center justify-center text-black p-5 hover:bg-white/20 duration-500 bg-white/40 h-10"
                    >
                      <Link
                        className="font-montserrat text-white"
                        href={animal.link}
                      >
                        Discover More
                      </Link>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </Slider>
        <div className="w-full h-1 mt-5 relative">
          <div
            className="h-full bg-white transition-width duration-500 ease-in-out"
            style={{ width: `${((currentSlide + 1) / totalSlides) * 100}%` }}
          ></div>
        </div>
      </div>
      <style jsx global>{`
        .slick-prev:before,
        .slick-next:before {
          display: none;
        }
      `}</style>
    </section>
  );
};

const SampleNextArrow = (props) => {
  const { className, style, onClick } = props;
  return (
    <div
      className={`${className} bg-black bg-opacity-50 rounded-full`}
      style={{
        ...style,
        width: '60px',
        height: '60px',
        right: '-20px',
      }}
      onClick={onClick}
    >
      <span className="text-white text-lg">&#x2192;</span>
    </div>
  );
};

const SamplePrevArrow = (props) => {
  const { className, style, onClick } = props;
  return (
    <div
      className={`${className} bg-black bg-opacity-50 rounded-full flex items-center justify-center`}
      style={{
        ...style,
        width: '60px',
        height: '60px',
        left: '-20px',
        zIndex: 1,
      }}
      onClick={onClick}
    >
      <span className="text-white text-lg">&#x2190;</span>
    </div>
  );
};

export default Carousel;
