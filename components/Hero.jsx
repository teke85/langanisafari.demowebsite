import { useState, useEffect, useRef } from 'react';
import gsap from 'gsap';
import { useGSAP } from '@gsap/react';
import BgVideo from './BgVideo';
import Header from './header/GsapHeader';
import ScrollDownArrow from './scrolldown/ScrollDownArrow';
import Logo from './Logo';
import BookingForm from './booking/BookingForm';
import Preloader from './Preloader';

const Hero = () => {
  // const [showPreloader, setShowPreloader] = useState(false);
  const comp = useRef(null);

  // useEffect(() => {
  //   const preloaderShown = localStorage.getItem('preloaderShown');
  //   console.log('Preloader shown:', preloaderShown);
  //   if (!preloaderShown) {
  //     setShowPreloader(true);
  //   }
  // }, []);

  useGSAP(() => {
    const t1 = gsap.timeline({
      scrolltrigger: {
        trigger: '.box',
        start: 'top',
        end: 'bottom',
        scrub: true,
      },
    });

    t1.fromTo(
      '.box',
      {
        clipPath: 'inset(45%)',
      },
      {
        clipPath: 'inset(0%)',
        opacity: 1,
        duration: 4,
        delay: 0.5,
      }
    );

    t1.to('.form', {
      opacity: 1,
      duration: 5,
      delay: 0.3,
    });

    t1.to('.wrapper', {
      opacity: 1,
    });

    t1.from(['#welcome', '#sub-title', '#paragraph'], {
      opacity: 0,
      y: '+=30',
      duration: 0.9,
      stagger: 1,
    });
  }, [comp]); // Empty dependency array means it will run only once

  return (
    <div ref={comp} className="flex relative h-[140vh] md:h-[100vh] flex-col">
      {/* {showPreloader && (
        <Preloader onComplete={() => setShowPreloader(false)} />
      )} */}
      <Header />
      <div className="box opacity-0 absolute inset-0 -z-10">
        <div className="absolute inset-0 bg-black/20 z-10" />
        <video className="w-full h-screen object-cover" autoPlay muted loop>
          <source src="assets/video/video.mp4" />
        </video>
      </div>
      <div className="flex flex-col h-screen justify-center items-center gap-24">
        <div className="wrapper opacity-0 bg-black/45 p-10 flex flex-col justify-center items-center gap-8">
          <h1
            id="welcome"
            className="font-bold font-lora italic leading-10 text-xl md:text-sm text-gray-200"
          >
            LANGANI SAFARI
          </h1>
          <h2
            id="sub-title"
            className="text-5xl font-aboreto text-gray-300 text-center md:text-3xl"
          >
            EXCLUSIVE SAFARI RESORT
          </h2>
          <p
            className="text-2xl text-center md:text-5xl font-ibarra text-white"
            id="paragraph"
          >
            A luxury experience, unlike the rest
          </p>
        </div>
      </div>
      <div className="flex -mb-[50px] w-full">
        <div className="flex justify-center form w-full opacity-0">
          <BookingForm />
        </div>
        {/* <ScrollDownArrow /> */}
      </div>
    </div>
  );
};

export default Hero;
