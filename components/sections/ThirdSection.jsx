import React from 'react';
import ExquisitePlaces from './exquisiteplace/ExquisitePlace';

const ThirdSection = () => {
  return (
    <section className="h-full flex items-center overflow-x-hidden justify-center">
      <div
        className="container-fluid space-y-20 p-5 md:pt-[100px] flex flex-col items-center justify-center w-full h-full bg-white
       text-black"
      >
        <h3 className="text-center font-ibarra text-4xl md:text-4xl w-9/12 uppercase">
          THE LANGANI PRIVATE VILLA COLLECTION
        </h3>
        <p className="font-montserrat text-[#777] text-center">
          Langani Safari offers a collection of luxurious open-plan private
          villas carefully designed to give our guests space and extra comfort.
          All our villas are specially spread out in the wilderness to offer
          privacy and tranquility for guests to experience the African sounds of
          nature. Each villa is crafted with floor-to-ceiling windows
          beautifully appointed with full views of the outside wildlife. Our
          interiors are finished with high end luxury furnishings sourced from
          across Africa.
        </p>
        <ExquisitePlaces />
      </div>
    </section>
  );
};

export default ThirdSection;
