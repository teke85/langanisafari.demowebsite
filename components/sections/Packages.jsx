import React, { useRef } from 'react';
import Heading from '../Heading';
import Image from 'next/image';
import Link from 'next/link';
import { gsap } from 'gsap';

const activities = [
  {
    image: '/assets/photos/guided_safari.jpg',
    title: 'Guided Safari Tours',
    description:
      'Join our expert guides on thrilling safari tours, exploring the rich wildlife and breathtaking landscapes of the African wilderness.',
    link: '#',
  },
  {
    image: '/assets/photos/winetasting.jpg',
    title: 'Wine Tasting',
    description:
      'Savor a selection of fine wines in an exclusive tasting session, guided by our knowledgeable sommeliers.',
    link: '#',
  },
  {
    image: '/assets/photos/food_6.jpg',
    title: 'African High Tea in The Wilderness $50',
    description:
      'Enjoy a beautiful high tea in the scenic wilderness. Our high tea includes freshly baked pastries and high tea sandwiches beautifully paired wth a variety of African teas to choose from Kids 12 and under $30.',
    link: '#',
  },
  {
    image: '/assets/photos/food_2.jpg',
    title: 'Langani Premium Package $100',
    description:
      'This package includes a 3-course gourmet meal, guided fishing and a beautiful guided safari with our experts. The Safari and fishing includes light bites and refreshments. Kids 12 and under $75.',
    link: '#',
  },
  {
    image: '/assets/photos/Food_17.jpg',
    title: 'Standard Package',
    description:
      'An unforgetable 3 course meal cooked by our international chef. Kids 12 and under $30',
    link: '#',
  },
];

const Packages = () => {
  const cardRefs = useRef([]);

  const addToRefs = (el) => {
    if (el && !cardRefs.current.includes(el)) {
      cardRefs.current.push(el);
    }
  };

  const handleMouseEnter = (index) => {
    const elements = cardRefs.current[index].querySelectorAll('.animate');
    gsap.to(elements, {
      y: 0,
      opacity: 1,
      stagger: 0.2,
      duration: 0.5,
    });
  };

  const handleMouseLeave = (index) => {
    const elements = cardRefs.current[index].querySelectorAll('.animate');
    gsap.to(elements, {
      y: 20,
      opacity: 0,
      stagger: 0.2,
      duration: 0.5,
    });
  };

  return (
    <section className="h-full bg-white">
      <div className="container-fluid space-y-20 p-5 md:py-20 flex flex-col items-center justify-center w-full h-full text-black">
        <div className="w-full h-full">
          <Heading text="A WORTHY GETAWAY." />
          <h3 className="font-aboreto text-center text-black text-3xl mb-10 md:text-4xl w-full uppercase">
            LANGANI PACKAGES
          </h3>
        </div>
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8 w-full">
          {activities.map((activity, index) => (
            <div
              key={index}
              className="p-4"
              ref={addToRefs}
              onMouseEnter={() => handleMouseEnter(index)}
              onMouseLeave={() => handleMouseLeave(index)}
            >
              <div className="wrapper bg-white overflow-hidden shadow-md transition-transform duration-500 transform hover:scale-105">
                <div className="relative w-full h-[70vh]">
                  <Image
                    src={activity.image}
                    alt={activity.title}
                    layout="fill"
                    objectFit="cover"
                    priority={true}
                    quality={100}
                  />
                  <div className="absolute p-10 inset-0 flex flex-col gap-5 justify-center items-center bg-black bg-opacity-50 opacity-0 hover:opacity-100 transition-opacity duration-500">
                    <h3 className="text-xl font-montserrat font-light text-white text-center mb-2 animate">
                      {activity.title}
                    </h3>
                    <p className="text-white font-aboreto text-center mb-4 animate">
                      {activity.description}
                    </p>
                    <button
                      id="button"
                      className="hidden md:flex font-medium text-[13px] border font-montserrat items-center justify-center text-black p-5 hover:bg-white/20 duration-500 bg-white/40 h-10"
                    >
                      <Link href={activity.link} className="text-white">
                        Discover More
                      </Link>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};

export default Packages;
