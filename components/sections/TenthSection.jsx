'use client';

import React from 'react';
import Image from 'next/image';

const images = [
  {
    image: '/assets/photos/entrance.jpg',
  },
  {
    image: '/assets/photos/outdoor_1.jpg',
  },
  // Add more places as needed
];

const TenthSection = () => {
  const leftImage = images[0];
  const rightImage = images[1];

  return (
    <section className="flex p-10 flex-col items-center justify-center h-[50vh] md:h-[100vh] py-5 w-full bg-[#F6F4EF]">
      <div className="container h-full w-full flex justify-between items-center">
        <div className="w-3/12 p-2 relative h-[250px]">
          <Image
            src={leftImage.image}
            alt={leftImage.title}
            width={150}
            height={150}
            quality={100}
          />
        </div>
        <div className="w-6/12 flex flex-col gap-3 items-center justify-center">
          <h3 className="font-semibold text-[8px] md:text-base text-center font-montserrat text-black">
            EXPERIENCE WHAT YOU HAVE ALWAYS WANTED
          </h3>
          <p className="text-black text-sm md:text-4xl font-ibarra text-center">
            LETS HAVE YOU HERE AND ENJOY THE FUN
          </p>
        </div>
        <div className="flex items-end p-2 justify-end w-3/12 relative h-[250px]">
          <Image
            src={rightImage.image}
            alt={rightImage.title}
            width={150}
            height={150}
            quality={100}
          />
        </div>
      </div>
    </section>
  );
};

export default TenthSection;
