import React from 'react';
import TestSlider from '@/components/TestSlider';

const TestimonialSection = () => {
  return (
    <section className="flex items-center justify-center w-full h-screen bg-white">
      <div className="container">
        <h3 className="text-black text-2xl md:text-4xl font-aboreto text-center w-full">
          Kind words from our clients
        </h3>
        <TestSlider />
      </div>
    </section>
  );
};

export default TestimonialSection;
