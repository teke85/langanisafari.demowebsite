import Image from 'next/image';
import Link from 'next/link';
import footerBg from '../../public/assets/photos/footer.png';
import { FaInstagram, FaFacebook, FaTwitter } from 'react-icons/fa';
import GoogleMapEmbed from '../GoogleMapEmbed';
import Logo from '../Logo';

const Footer = () => {
  return (
    <footer className="relative h-[120vh] md:h-screen text-white">
      <div className="absolute inset-0">
        <Image
          src={footerBg}
          alt="Footer Background"
          layout="fill"
          objectFit="cover"
          quality={100}
          className="z-0"
        />
        <div className="absolute inset-0 bg-black opacity-50 z-10"></div>
      </div>
      <div className="relative z-20 container mx-auto p-8 h-full flex items-end">
        <div className="w-full">
          <div className="grid grid-cols-1 md:grid-cols-4 gap-8">
            <div className="flex flex-col items-start justify-center gap-5">
              <span className="font-aboreto font-semibold">
                BOOKING & RESERVATIONS
              </span>
              <span className="font-montserrat font-light">Online Booking</span>
              <span className="font-light font-montserrat">Book by email</span>
              <span className="font-light font-montserrat">Book by phone</span>
            </div>
            <div className="flex flex-col items-start justify-center gap-5">
              <span className="font-aboreto font-semibold">AMENITIES</span>
              <span className="font-montserrat font-light">Our Rooms</span>
              <span className="font-light font-montserrat">
                Food & Beverage
              </span>
              <span className="font-light font-montserrat">Front Desk</span>
            </div>
            <div className="flex flex-col items-start justify-center gap-5">
              <span className="font-aboreto font-semibold">ABOUT</span>
              <span className="font-montserrat font-light">
                Story & History
              </span>
              <span className="font-montserrat font-light">FAQ</span>
              <span className="font-montserrat font-light">Contact Us</span>
            </div>
            <div className="flex flex-col items-start md:items-center gap-5">
              <span className="font-aboreto font-semibold">FOLLOW US</span>
              <div className="flex space-x-10">
                <Link href="https://www.instagram.com/langanisafari">
                  <FaInstagram className="text-2xl text-[#ECECEC]" />
                </Link>
                <Link href="/https://web.facebook.com/profile.php?id=61554289924623">
                  <FaFacebook className="text-2xl text-[#ECECEC]" />
                </Link>
                <Link href="/">
                  <FaTwitter className="text-2xl text-[#ECECEC]" />
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="flex items-center justify-between px-10 w-full z-30">
        <div className="text-white font-aboreto">
          © 2024 LANGANI LUXURY SAFARI DEMO WEBSITE
          <br />
          ALL VIDEOS/IMAGES USED ON THIS SITE ARE FOR DEMO PURPOSES ONLY
        </div>
        <div>
          <Image
            className="relative"
            src="/assets/logos/alpha.png"
            alt="picture of the langani logo in black"
            width={100}
            height={100}
            priority
          />
        </div>
      </div>
    </footer>
  );
};

export default Footer;
