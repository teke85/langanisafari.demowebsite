import Image from 'next/image';
import Link from 'next/link';

const places = [
  {
    image: '/assets/photos/LanganiRoom_1.jpg',
    title: 'LUXURY VILLA OVERNIGHT SPECIAL',
    description: '8 ADULTS 8 CHILDREN',
    link: '/room-details',
  },
  {
    image: '/assets/photos/room_2.jpg',
    title: 'LUXURY VIP VILLA SPECIAL',
    description: '8 ADULTS 8 CHILDREN',
    link: '/room-details',
  },
  // Add more places as needed
];

const ExquisitePlaces = () => {
  return (
    <section className="py-1 px-0 md:py-12 w-full">
      <div className="container flex flex-col items-center mx-auto">
        <div className="flex items-center w-full py-1 md:py-5">
          <div className="grid w-full grid-cols-1 md:grid-cols-2 gap-10">
            {places.map((place, index) => (
              <div
                key={index}
                className="flex flex-col items-center overflow-hidden"
              >
                <div className="relative w-full h-64 md:w-9/12 md:h-[500px] transition-transform duration-700 transform hover:scale-105">
                  <Image
                    src={place.image}
                    alt={place.title}
                    layout="fill"
                    objectFit="cover"
                    priority={true}
                    quality={100}
                  />
                </div>
                <div className="p-6">
                  <h3 className="text-2xl uppercase font-aboreto font-light mb-3">
                    {place.title}
                  </h3>
                  <p className="text-gray-700 font-ibarra font-light text-sm mb-6">
                    {place.description}
                  </p>
                  <button className="w-full">
                    <Link
                      href={place.link}
                      className="text-black text-sm w-6/12 font-montserrat border border-black py-1 rounded-2xl flex justify-center items-center"
                    >
                      Book Stay
                      <svg
                        className="w-4 h-4 ml-2"
                        fill="none"
                        stroke="currentColor"
                        viewBox="0 0 24 24"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth="2"
                          d="M9 5l7 7-7 7"
                        ></path>
                      </svg>
                    </Link>
                  </button>
                </div>
              </div>
            ))}
          </div>
        </div>
        <button>
          <Link
            className="flex font-montserrat text-white bg-black p-4 font-semibold items-center mt-10 md:mt-0"
            href="/villas"
          >
            DISCOVER VILLAS
            <svg
              className="w-4 h-4 ml-2"
              fill="none"
              stroke="currentColor"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M9 5l7 7-7 7"
              ></path>
            </svg>
          </Link>
        </button>
      </div>
    </section>
  );
};

export default ExquisitePlaces;
