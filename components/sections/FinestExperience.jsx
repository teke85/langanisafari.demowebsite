import React, { useRef } from 'react';
import gsap from 'gsap';
import { ScrollTrigger } from 'gsap/ScrollTrigger';
import { useGSAP } from '@gsap/react';
import { Suspense } from 'react';

const FinestExperience = () => {
  useGSAP(() => {
    gsap.registerPlugin(ScrollTrigger);
    const tl = gsap.timeline({
      scrollTrigger: {
        trigger: '.heading',
        toggleActions: 'restart none none reset',
        start: 'top 50%',
        end: 'bottom 40%',
        scrub: true, // smooth animation when scrolling
      },
    });

    tl.fromTo(
      '.h3',
      {
        ease: 'power1.inOut',
        opacity: 0,
      },
      { opacity: 1, y: '50', duration: 3 }
    );
  }, []);

  return (
    <section className="h-full flex items-center overflow-x-hidden justify-center">
      <div className="container-fluid space-y-10 p-5 md:py-10 flex flex-col justify-center w-full h-full bg-softwhite text-black">
        <div className="w-full">
          <h3 className="heading font-montserrat text-start text-[#B0B0B0] font-semibold font-sm">
            THE FINEST EXPERIENCES.
          </h3>
        </div>
        <h3 className="text-start font-aboreto leading-tight text-black text-3xl md:text-2xl uppercase text-link-item-holder">
          Langani Safari is the only privately owned game ranch That Has The
          Largest Population Of Rhinoceroses In Zambia.
        </h3>
        <div className="grid grid-cols-1 md:grid-cols-2 gap-4 mt-10">
          <div className="relative md:w-3/4 h-screen md:h-[80vh]">
            <video
              preload="none"
              className="w-12/12 md:w-full h-full object-cover"
              autoPlay
              muted
              loop
            >
              <source src="assets/video/video.mp4" />
            </video>
            <div className="absolute inset-0 bg-black bg-opacity-50 flex flex-col gap-4 items-center justify-center opacity-0 hover:opacity-100 transition-opacity duration-700">
              <h3 className="text-white text-center text-3xl font-aboreto">
                VISIT ZAMBIA
              </h3>
              <span className="text-white text-center text-sm font-montserrat px-10">
                Langani means “To See” in Nyanja, the local Zambian Language.
                Langani Ranch is designed and owned by a successful business man
                and his wife. They are a well-traveled Zambian Family with
                in-depth knowledge of Africa, wildlife, and international luxury
                experiences. They have both been in the hospitality industry for
                more than 20 years and have two well-established sister hotels,
                Chrismar Hotels, located in Lusaka City and in Livingstone,
                right by the Victoria Falls.
              </span>
            </div>
          </div>
          <div className="relative md:justify-self-end h-screen md:h-[80vh] md:w-3/4 w-full">
            <video
              preload="none"
              className="w-12/12 md:w-full h-full object-cover"
              autoPlay
              muted
              loop
            >
              <source src="assets/video/video_1.mov" />
              Your browser does not support the video tag.
            </video>
            <div className="absolute inset-0 bg-black bg-opacity-50 px-10 flex flex-col gap-4 items-center justify-center opacity-0 hover:opacity-100 transition-opacity duration-300">
              <h3 className="text-white text-center font-aboreto text-3xl">
                THE LANGANI STORY
              </h3>
              <span className="text-white text-center text-sm font-light font-montserrat">
                Zambia is a peaceful African country with so much joy and
                kind-hearted people. Our wish is for guests to come and
                experience the rich history, culture, and wilderness. Zambia has
                a rich history; in 1924 – British Colonial Office took control
                of Northern Rhodesia, known as Zambia today. Livingstone, where
                the Victoria Falls and sister hotel, Chrismar, is located, was
                chosen as the first capital. Northern Rhodesia developed into
                one of the leading producers of copper in the world. After many
                years of colonization, Northern Rhodesia gained independence
                under a black Government and a new name, Zambia, in 1969. Today,
                Zambia is still recognized for its copper and is one of the most
                water-rich countries in Africa; Zambia has five vast lakes,
                three major rivers, at least 17 waterfalls, and various wetland
                areas. In addition to major conservation areas, Zambia offers
                well-rounded safari experiences in the raw African wilderness.
              </span>
            </div>
          </div>
        </div>
        <p className="text-center font-montserrat text-base">
          Travelers from all over choose to stay at Langani due to the vast,
          beautiful and remote location.
          <br />
          Our guests appreciate the restorative effects of the pristine
          location, as well as the limited guest and vehicle numbers, consistent
          game viewing and exceptional care they receve during their stay.
          <br />
          When its time to depart, guests leave transformed for life, deeply
          moved by their connection and contribution to the legacy of this
          continent.
        </p>
      </div>
    </section>
  );
};

export default FinestExperience;
