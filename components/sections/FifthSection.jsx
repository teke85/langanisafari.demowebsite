'use client';

import React from 'react';
import Link from 'next/link';
import Image from 'next/image';

const Dining = [
  {
    image: '/assets/photos/food_1.jpg',
    title: 'Champagne Breakfast in the Wild',
    description:
      'Indulge in an exquisite breakfast experience like no other with our Champagne Breakfast in the Wild. Imagine starting your day with a gourmet meal, perfectly complemented by a bottle of fine champagne, all set against the breathtaking backdrop of the African wilderness. Savor fresh, locally-sourced ingredients prepared by our expert chefs, as you bask in the serenity of nature. Whether celebrating a special occasion or simply treating yourself to a touch of luxury, this is a moment of pure indulgence that will stay with you forever. Raise your glass to an unforgettable start to your safari adventure.',
    link: '#',
  },
  // Other places...
];

const FifthSection = () => {
  return (
    <section className="py-5 w-full bg-white">
      <div className="container mx-auto px-4 h-full w-full">
        {Dining.map((Dining, index) => (
          <div
            key={index}
            className="grid grid-cols-1 md:grid-cols-2 items-center gap-10 py-10 justify-center w-full rounded-lg overflow-hidden"
          >
            <div className="relative w-[3/12] md:w-9/12 h-screen transition-transform duration-200 transform hover:scale-105 order-1 md:order-2">
              <Image
                src={Dining.image}
                alt={Dining.title}
                layout="fill"
                objectFit="cover"
                priority={true}
                quality={80}
              />
            </div>
            <div className="w-12/12 md:w-9/12 pt-20 flex flex-col justify-center order-2 md:order-1">
              <h3 className="text-2xl font-semibold font-aboreto text-black mb-4">
                {Dining.title}
              </h3>
              <p className="text-[#777] font-montserrat text-md mb-6">
                {Dining.description}
              </p>
              <button className="w-full">
                <Link
                  href={Dining.link}
                  className="text-black text-sm w-9/12 font-montserrat border border-black py-1 rounded-2xl flex justify-center items-center"
                >
                  EXPLORE RESTAURANT
                  <svg
                    className="w-4 h-4 ml-2"
                    fill="none"
                    stroke="currentColor"
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M9 5l7 7-7 7"
                    ></path>
                  </svg>
                </Link>
              </button>
            </div>
          </div>
        ))}
      </div>
    </section>
  );
};

export default FifthSection;
