'use client';

import React from 'react';
import Link from 'next/link';
import Image from 'next/image';
import { motion, useInView } from 'framer-motion';

const Meals = [
  {
    image: '/assets/photos/Food_17.jpg',
    title: 'A Delectable Culinary Masterpiece',
    description:
      'The entire plate is a feast for the eyes, with a harmonious balance of colors and textures that promise a delightful culinary experience.',
    link: '#',
  },
];

const SeventhSection = () => {
  return (
    <section className="py-5 w-full bg-softwhite">
      <div className="container mx-auto px-4 h-full w-full">
        {Meals.map((meal, index) => (
          <ImageReveal key={index} meal={meal} />
        ))}
      </div>
    </section>
  );
};

const ImageReveal = ({ meal }) => {
  const ref = React.useRef(null);
  const isInView = useInView(ref, { amount: 0.5 });

  return (
    <div className="grid grid-1 md:grid-cols-2 items-center gap-10 py-10 justify-center w-full rounded-lg overflow-hidden">
      <div className="p-6 w-12/12 md:w-12/12 py-20 flex flex-col justify-center">
        <h3 className="text-2xl font-semibold font-aboreto text-black mb-4">
          {meal.title}
        </h3>
        <p className="text-gray-700 font-montserrat text-md mb-6">
          {meal.description}
        </p>
        <button className="w-full">
          <Link
            href={meal.link}
            className="text-black text-sm font-montserrat border-2 border-black py-2 rounded-2xl flex justify-center items-center"
          >
            EXPLORE RESTAURANT
            <svg
              className="w-4 h-4 ml-2"
              fill="none"
              stroke="currentColor"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M9 5l7 7-7 7"
              ></path>
            </svg>
          </Link>
        </button>
      </div>
      <motion.div
        ref={ref}
        initial={{ opacity: 0 }}
        animate={isInView ? { opacity: 1 } : { y: 0 }}
        viewport={{ once: true }}
        transition={{ duration: 0.5, ease: 'easeIn' }}
        className="relative w-12/12 p-10 md:w-9/12 h-full transition-transform duration-200 transform hover:scale-105 overflow-hidden"
      >
        <Image
          src={meal.image}
          alt={meal.title}
          layout="fill"
          objectFit="cover"
          priority={true}
        />
      </motion.div>
    </div>
  );
};

export default SeventhSection;
