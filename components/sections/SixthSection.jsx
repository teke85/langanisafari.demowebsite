'use client';

import React from 'react';
import Link from 'next/link';
import Image from 'next/image';
import GridComponent from '../GridComponent';

const Dining = [
  {
    image: '/assets/photos/room_1.jpg',
    title: 'Beautiful Beach House',
    description:
      'Enjoy a luxurious stay at our beautiful beach house with stunning ocean views.',
    link: '#',
  },
  {
    image: '/assets/photos/room_2.jpg',
    title: 'Mountain Retreat',
    description:
      'Escape to our peaceful mountain retreat, surrounded by nature.',
    link: '#',
  },
  {
    image: '/assets/photos/room_3.jpg',
    title: 'Mountain Retreat',
    description:
      'Escape to our peaceful mountain retreat, surrounded by nature.',
    link: '#',
  },
  // Add more places as needed
];

const SixthSection = () => {
  return (
    <section className="py-5 w-full bg-white">
      {/* <GridComponent /> */}
    </section>
  );
};

export default SixthSection;
