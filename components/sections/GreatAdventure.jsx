"use client";

import React, { useEffect } from "react";
import Image from "next/image";
import { motion, useAnimation, useInView } from "framer-motion";

const Experiences = [
  {
    image: "/assets/photos/zebra.jpg",
    title: "Gather Around the Fire Under the African Sky",
    content:
      "Langani Safaris vegetable garden is an ecosystem intended to be sustainable and self-sufficient. We grow most of our produce on the ranch, where we pride ourselves on ingredients for freshness. All our other produce is locally sourced in support of small Zambian businesses.",
    link: "#",
  },
  // Add more experiences here if needed...
];

const imageVariants = {
  hidden: { opacity: 0 },
  visible: { opacity: 1, transition: { duration: 2.5 } },
};

const contentVariants = {
  hidden: { opacity: 0, y: 50 },
  visible: { opacity: 1, y: 0, transition: { duration: 1, ease: "easeInOut" } },
};

const FourthSection = () => {
  const ref = React.useRef(null);
  const inView = useInView(ref, { amount: 0.5 });
  const controls = useAnimation();

  useEffect(() => {
    if (inView) {
      controls.start("visible");
    }
  }, [controls, inView]);

  return (
    <section className="flex items-center min-h-screen py-5 bg-white">
      <div className="container mx-auto px-4">
        {Experiences.map((experience, index) => (
          <div
            key={index}
            className="grid grid-cols-1 md:grid-cols-2 gap-4 my-5"
          >
            <div className="text-box">
              <h5 className="text-gray-500 uppercase font-semibold font-montserrat text-sm mb-2">
                A Great Adventure Begins Here
              </h5>
              <h2 className="text-5xl font-aboreto text-black mb-4">
                We strive only for the best!
              </h2>
            </div>
            <div
              className="relative flex flex-col gap-10 items-center"
              ref={ref}
            >
              <motion.div
                className="relative w-full h-96 md:h-[50vh] overflow-hidden"
                initial="hidden"
                animate={controls}
                variants={imageVariants}
                viewport={{ once: true }}
              >
                <Image
                  src={experience.image}
                  alt={experience.title}
                  layout="fill"
                  objectFit="cover"
                  className="absolute inset-0 z-0"
                  priority={true}
                  quality={100}
                />
              </motion.div>
              <motion.div
                className="flex items-center justify-center md:absolute z-10 bg-black/90 text-white p-6 md:w-10/12 w-12/12 h-96 md:h-[50vh] md:top-[160px] md:right-[400px]"
                initial="hidden"
                animate={controls}
                variants={contentVariants}
                viewport={{ once: true }}
              >
                <p className="font-montserrat text-center">
                  {experience.content}
                </p>
              </motion.div>
            </div>
          </div>
        ))}
      </div>
    </section>
  );
};

export default FourthSection;
