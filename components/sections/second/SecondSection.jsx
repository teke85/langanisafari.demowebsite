// components/SecondSection.js
import InfiniteScroll from '../../infiniteScroll/InfiniteScroll';
import './secondsection.css';
import { useRef } from 'react';
import gsap from 'gsap';
import { ScrollTrigger } from 'gsap/ScrollTrigger';
import { useGSAP } from '@gsap/react';

gsap.registerPlugin(ScrollTrigger);

const SecondSection = () => {
  const containerRef = useRef(null);
  const headingRef = useRef(null);
  const subheadingRef = useRef(null);
  const paragraphRef = useRef(null);

  useGSAP(
    () => {
      const elements = [
        headingRef.current,
        subheadingRef.current,
        paragraphRef.current,
      ];

      // Set initial clip-path values
      gsap.set(elements, { clipPath: 'inset(0 0 100% 0)' });

      const tl = gsap.timeline({
        scrollTrigger: {
          trigger: containerRef.current,
          start: 'top 80%', // Adjust the start point to trigger earlier
          end: 'bottom top',
          toggleActions: 'play none none none', // Ensure it only plays once
        },
      });

      tl.to(headingRef.current, {
        clipPath: 'inset(0 0 0% 0)',
        duration: 1.1,
        ease: 'power3.out',
      })
        .to(
          subheadingRef.current,
          { clipPath: 'inset(0 0 0% 0)', duration: 1.3, ease: 'power3.out' },
          '+=0.8'
        )
        .to(
          paragraphRef.current,
          { clipPath: 'inset(0 0 0% 0)', duration: 1.5, ease: 'power3.out' },
          '+=0.8'
        );
    },
    { scope: containerRef }
  );

  return (
    <section className="h-full flex items-center overflow-x-hidden justify-center">
      <div
        ref={containerRef}
        className="overflow-hidden container-fluid space-y-16 p-5 md:py-20 flex flex-col items-center justify-center w-full h-full bg-softwhite text-black"
      >
        <div className="w-full">
          <h3
            ref={headingRef}
            className="heading font-montserrat text-start text-black/70 font-semibold font-sm mt-20 md:mt-0"
          >
            A LUXURY SAFARI, AT THE BEST PRICE.
          </h3>
        </div>
        <h3
          ref={subheadingRef}
          className="text-center font-aboreto text-black text-3xl md:text-4xl w-9/12 uppercase"
        >
          The day I woke up, I was filled with inspiration. This remarkable day
          began when I opened my eyes in a unique African luxury resort like no
          other.
        </h3>
        <p
          ref={paragraphRef}
          className="text-2 text-center text-[#777] font-montserrat text-base w-10/12"
        >
          Newly built with no aspect left unattended, this Luxury Safari blends
          style and comfort seamlessly with nature. This recently established
          Luxury Ranch is built in the wilderness with glass walls and open-air
          spaces, allowing you to appreciate your surroundings amid the African
          bush that has Rhinoceroses, African Buffalos, Giraffes, Lions and many
          more famously recognized African animals roaming around.
        </p>
        <div className="flex items-center justify-center mt-10">
          <InfiniteScroll />
        </div>
      </div>
    </section>
  );
};

export default SecondSection;
