import React from 'react'
import HorizontalScroll from '../HorizontalScroll';

const HorizontalSection = () => {
  return (
    <section className="w-full h-full bg-white">
      <div className="container-fluid">
        <div className="flex flex-col gap-4 px-10">
          <h3 className="font-light text-center text-black font-aboreto text-4xl">
            Big 5: African Elephant, Lion, Leopard, Cape Buffalo, and
            Rhinoceros.
          </h3>
          <p className="text-[#777] text-center font-montserrat">
            Langani Ranch has 3 of the Big 5 animals: Rhinos, Buffalos, and
            Kings of the jungle, Lions. In addition, we have immense numbers of
            famously recognized African animals, such as giraffes, zebras,
            impalas, and many more. It is guaranteed that you will have an
            intimate sighting with all our animals.
          </p>
          <p className="text-[#777] text-center font-montserrat">
            We pride ourselves on the immense collection of animals on our
            Ranch; that is why at Langani Safari, we encourage all our guests to
            have a Safari Journey, as it will awaken your spirit, and our
            deepest wish is to give all our guests a chance to experience this
            African Fire. Langani Ranch has two dams where you can watch the
            animals drink water. In addition, we also offer relaxed water-based
            activities. We offer tailored Safaris to give our guests
            unforgettable experiences. We also offer guided hikes, fishing in
            African nature, Pedal-Boating, Zambian Sunset cocktails, wine
            tasting, and many more activities.
          </p>
          <HorizontalScroll />
        </div>
      </div>
    </section>
  );
}

export default HorizontalSection
