'use client';

import React from 'react';
import Link from 'next/link';
import Image from 'next/image';
import SliderComponent from '../slider/slider';

const NinethSection = () => {
  return (
    <section className="flex p-10 flex-col items-center justify-center h-[100vh] py-5 w-full bg-[#F6F4EF]">
      <h3 className="font-playfair text-center text-black text-5xl">
        BEAUTIFUL WORDS FROM OUR GUESTS
      </h3>
    </section>
  );
};

export default NinethSection;
