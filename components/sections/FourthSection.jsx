'use client';

import React from 'react';
import Link from 'next/link';
import Image from 'next/image';

const Experiences = [
  {
    image: '/assets/photos/outdoor_fire.jpg',
    title: 'Gather Around the Fire Under the African Sky',
    description:
      'Experience the magic of the African wilderness as you gather around a crackling outdoor fire. Let the warmth of the flames and the scent of burning wood envelop you, creating an unforgettable atmosphere. Our safari resort offers a unique opportunity to unwind under the vast, star-studded sky, where stories are shared, and memories are made. Immerse yourself in the serene beauty of nature, listen to the sounds of the wild, and enjoy the camaraderie of fellow travelers. This is more than just a moment; it’s an experience that captures the essence of the safari spirit.',
    link: '#',
  },
];

const FourthSection = () => {
  return (
    <section className="py-5 w-full bg-white">
      <div className="container mx-auto px-4 h-full w-full">
        {Experiences.map((experience, index) => (
          <div
            key={index}
            className="grid grid-1 md:grid-cols-2 items-center gap-10 py-10 justify-center w-full rounded-lg overflow-hidden"
          >
            <div className="relative w-[3/12] md:w-9/12 h-screen transition-transform duration-200 transform hover:scale-105">
              <Image
                src={experience.image}
                alt={experience.title}
                layout="fill"
                objectFit="cover"
                fill
                priority={true}
                quality={100}
              />
            </div>
            <div className="w-12/12 md:w-9/12 pt-20 flex flex-col justify-center">
              <h3 className="text-2xl font-semibold font-aboreto text-black mb-4">
                {experience.title}
              </h3>
              <p className="text-[#777] font-montserrat text-md mb-6">
                {experience.description}
              </p>
              <button className="w-full">
                <Link
                  href={experience.link}
                  className="text-black text-sm md:w-9/12 font-montserrat border border-black py-1 rounded-2xl flex justify-center items-center"
                >
                  EXPLORE EXPERIENCES
                  <svg
                    className="w-4 h-4 ml-2"
                    fill="none"
                    stroke="currentColor"
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M9 5l7 7-7 7"
                    ></path>
                  </svg>
                </Link>
              </button>
            </div>
          </div>
        ))}
      </div>
    </section>
  );
};

export default FourthSection;
