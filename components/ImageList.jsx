'use client';

import React from 'react';
import Image from 'next/image';
import gsap from 'gsap';
import { useGSAP } from '@gsap/react';
import { ScrollTrigger } from 'gsap/dist/ScrollTrigger';

const ImageList = () => {
  useGSAP(() => {
    gsap.registerPlugin(ScrollTrigger);
    const tl = gsap.timeline({
      scrollTrigger: {
        trigger: '.image1',
        toggleActions: 'restart none none reset',
        start: 'top 50%',
        end: 'bottom 40%',
        scrub: true, // smooth animation when scrolling
        // markers: {
        //   startColor: 'white',
        //   endColor: 'white',
        // },
      },
    });

    tl.fromTo(
      '.image1',
      {
        ease: 'power1.inOut',
        opacity: 0,
      },
      { opacity: 1, y: '50', duration: 3 }
    );
    
  }, []);

  return (
    <div className="flex flex-col gap-20 items-center h-full">
      <Image
        src={'https://picsum.photos/600/400?random=1'}
        alt="smooth scroll"
        width={600}
        height={400}
        className="image1"
      />
      <Image
        src={'https://picsum.photos/600/400?random=2'}
        alt="smooth scroll"
        width={600}
        height={400}
        className="image2"
      />
      <Image
        src={'https://picsum.photos/600/400?random=3'}
        alt="smooth scroll"
        width={600}
        height={400}
        className="image3"
      />
      <Image
        src={'https://picsum.photos/600/400?random=4'}
        alt="smooth scroll"
        width={600}
        height={400}
      />
      <Image
        src={'https://picsum.photos/600/400?random=5'}
        alt="smooth scroll"
        width={600}
        height={400}
      />
      <Image
        src={'https://picsum.photos/600/400?random=6'}
        alt="smooth scroll"
        width={600}
        height={400}
      />
      <Image
        src={'https://picsum.photos/600/400?random=7'}
        alt="smooth scroll"
        width={600}
        height={400}
      />
      <Image
        src={'https://picsum.photos/600/400?random=8'}
        alt="smooth scroll"
        width={600}
        height={400}
      />
      <Image
        src={'https://picsum.photos/600/400?random=9'}
        alt="smooth scroll"
        width={600}
        height={400}
      />
    </div>
  );
};

export default ImageList;
