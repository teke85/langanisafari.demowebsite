'use client';

import Hero from '@/components/Hero';
import SecondSection from '@/components/sections/second/SecondSection';
import ThirdSection from '@/components/sections/ThirdSection';
import FourthSection from '@/components/sections/FourthSection';
import FifthSection from '@/components/sections/FifthSection';
import SixthSection from '@/components/sections/SixthSection';
import Packages from '@/components/sections/Packages';
import FinestExperience from '@/components/sections/FinestExperience';
import TenthSection from '@/components/sections/TenthSection';
import GreatAdventure from '@/components/sections/GreatAdventure';
import Carousel from '@/components/horizontalSlider/HorizontalSlider';
import './globals.css';
import HorizontalSection from '@/components/sections/HorizontalSection';
import TestimonialSection from '@/components/sections/TestimonialSection';
import HoverImageComponent from '@/components/HoverImage';

function Home() {
  return (
    <main className="flex w-full min-h-screen flex-col overflow-hidden">
      <Hero />
      <SecondSection />
      <GreatAdventure />
      <HoverImageComponent />
      <ThirdSection />
      <FourthSection />
      <FifthSection />
      <SixthSection />
      <HorizontalSection />
      <Carousel />
      <Packages />
      <FinestExperience />
      <TestimonialSection />
      <TenthSection />
    </main>
  );
}

export default Home;
