import React from 'react';

const AboutUs = () => {
  return <section className="min-h-screen w-full bg-slate-600">About us page<sup>(02)</sup></section>;
};

export default AboutUs;
