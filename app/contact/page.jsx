import React from 'react';

const Contact = () => {
  return (
    <section className="min-h-screen w-full bg-slate-600">
      Contact page<sup>(04)</sup>
    </section>
  );
};

export default Contact;
