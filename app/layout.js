import {
  Montserrat,
  Lora,
  Poppins,
  Ubuntu,
  Ibarra_Real_Nova,
  Aboreto,
} from 'next/font/google';
import localFont from 'next/font/local';
import Footer from '../components/sections/FooterSection';
import './globals.css';
import SmoothScroll from '@/components/smoothscroll/SmoothScroll';

const ENCHANTS = localFont({
  src: '../public/assets/fonts/ENCHANTS.otf',
  variable: '--enchants',
  display: 'swap',
});

const poppins = Poppins({
  subsets: ['latin'],
  weight: ['400', '500', '700', '800'],
  variable: '--font-poppins',
  display: 'swap',
});

const aboreto = Aboreto({
  subsets: ['latin'],
  weight: ['400'],
  variable: '--font-aboreto',
  display: 'swap',
});

const ibarra = Ibarra_Real_Nova({
  subsets: ['latin'],
  weight: ['400', '500', '700'],
  variable: '--font-ibarra',
  display: 'swap',
});

const ubuntu = Ubuntu({
  subsets: ['latin'],
  weight: ['300', '400', '500', '700'],
  variable: '--font-ubuntu',
  display: 'swap',
});

const Roashe = localFont({
  src: '../public/assets/fonts/Roashe.otf',
  variable: '--Roashe',
  display: 'swap',
});

const lora = Lora({
  subsets: ['latin'],
  variable: '--font-lora',
  weight: '500',
  display: 'swap',
});

const montserrat = Montserrat({
  subsets: ['latin'],
  variable: '--font-montserrat',
});

// const playfair = Playfair({
//   subsets: ['latin'],
//   variable: '--font-playfair',
//   weight: '500',
//   display: 'swap',
// });

export const metadata = {
  title: 'Langani Safari - Safari Resort, Chisamba Zambia',
  description: 'Welcome to Langani Safari',
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body
        className={`${montserrat.variable} ${aboreto.variable} ${ibarra.variable} ${ubuntu.variable} ${poppins.variable} ${Roashe.variable} ${lora.variable}`}
      >
        <SmoothScroll>
          {children}
          <Footer />
        </SmoothScroll>
        <script src="//code.jivosite.com/widget/p7nHQqxYUT" async></script>
      </body>
    </html>
  );
}
