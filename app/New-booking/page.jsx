import Booking from '@/components/booking/BookingForm';
import React from 'react';

const BookingForm = () => {
  return (
    <section className="flex justify-center items-center w-full">
      <Booking />
    </section>
  );
};

export default BookingForm;
