"use client";

import React from "react";

const Villa = () => {
  return <div>Villa</div>;
};

export default Villa;

// import VillaList from '@/components/Villas/VillaList';

// const getRooms = async () => {
//   const res = await fetch(
//     `${process.env.NEXT_PUBLIC_API_URL}/villas?populate=*`,
//     {
//       next: {
//         revalidate: 0,
//       },
//     }
//   );
//   return await res.json();
// };

// const Villas = async () => {
//   const villas = await getRooms();
//   return (
//     <section className="h-full bg-white">
//       <div className="container mx-auto h-full">
//         <VillaList villas={villas} />
//       </div>
//     </section>
//   );
// };

// export default Villas;
