"use client";

const Villa = () => {
  return <div>Villa</div>;
};

export default Villa;

// import Reservation from '@/components/ui/Reservation';
// import Image from 'next/image';
// import { TbArrowsMaximize, TbUsers } from 'react-icons/tb';

// const getRoomData = async ({ params }) => {
//   try {
//     const res = await fetch(
//       `${process.env.NEXT_PUBLIC_API_URL}/villas/${params.id}?populate=*`,
//       {
//         next: {
//           revalidate: 0,
//         },
//       }
//     );

//     // Check if response is not OK
//     if (!res.ok) {
//       throw new Error(`Error: ${res.statusText}`);
//     }

//     return await res.json();
//   } catch (error) {
//     console.error('Failed to fetch room data:', error);
//     // Return default or empty data structure
//     return { data: { attributes: {} } };
//   }
// };

// const getReservationData = async () => {
//   try {
//     const res = await fetch(
//       `${process.env.NEXT_PUBLIC_API_URL}/reservations?populate=*`,
//       {
//         next: {
//           revalidate: 0,
//         },
//       }
//     );

//     // Check if response is not OK
//     if (!res.ok) {
//       throw new Error(`Error: ${res.statusText}`);
//     }

//     return await res.json();
//   } catch (error) {
//     console.error('Failed to fetch reservation data:', error);
//     // Return empty reservations or default structure
//     return { data: [] };
//   }
// };

// const VillaDetails = async ({ params }) => {
//   const villa = await getRoomData({ params });
//   const reservations = await getReservationData();

//   const imgURL = `${villa.data.attributes.image.data.attributes.url}`;
//   console.log(imgURL);

//   return (
//     <section className="w-full h-full bg-white py-20">
//       <div className="mx-auto container gap-10 h-full flex text-black">
//         <div className="flex flex-col space-y-12 md:flex-row md:space-x-12 w-full h-full">
//           <div className="w-full lg:w-8/12 flex flex-col">
//             <div className="relative h-[360px] lg:h-[420px] mb-8">
//               <Image
//                 src={imgURL}
//                 fill
//                 className="object-cover"
//                 alt={villa.data.attributes.name || 'Villa Image'}
//               />
//             </div>
//             <div>
//               {/* title & price */}
//               <div className="flex justify-between items-center mb-4">
//                 <h3 className="font-aboreto font-light">
//                   {villa.data.attributes.name || 'Villa Name'}
//                 </h3>
//                 <p className="font-montserrat font-bold">
//                   ${villa.data.attributes.pricePerNight || '0'}
//                   <span className="font-ibarra font-light">/ night</span>
//                 </p>
//               </div>
//               {/* info */}
//               <div>
//                 <div className="flex gap-10 mb-4">
//                   <div className="flex flex-col md:flex-row items-center gap-5">
//                     <TbArrowsMaximize />
//                     <p className="text-sm md:text-base font-ibarra">
//                       {villa.data.attributes.size || 'Size'} m<sup>2</sup>
//                     </p>
//                   </div>
//                   <div className="flex flex-col md:flex-row items-center gap-5">
//                     <TbUsers />
//                     <p className="text-sm md:text-base font-ibarra">
//                       {villa.data.attributes.capacity || 'Capacity'} Adults/
//                       {villa.data.attributes.capacity || 'Capacity'} Children
//                     </p>
//                   </div>
//                 </div>
//                 <div>
//                   <p className="font-ibarra">
//                     {villa.data.attributes.description || 'Description'}
//                   </p>
//                 </div>
//               </div>
//             </div>
//           </div>
//           <div className="flex items-center justify-center w-full lg:w-4/12 bg-slate-400">
//             <Reservation reservations={reservations} villa={villa} />
//           </div>
//         </div>
//       </div>
//     </section>
//   );
// };

// export default VillaDetails;
