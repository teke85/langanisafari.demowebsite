import Header from '@/components/header/GsapHeader';
import Image from 'next/image';
import React from 'react';

const RoomDetails = () => {
  return (
    <section className="bg-soft">
      <div className="container-fluid">
        <Header />
        {/* <header className="flex justify-between items-center p-6">
          <h1 className="text-4xl font-bold">SUITES</h1>
          <button className="text-2xl text-red-500">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
              className="w-8 h-8"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth={2}
                d="M4 6h16M4 12h16m-7 6h7"
              />
            </svg>
          </button>
        </header> */}
        <main className="flex flex-col items-center">
          <h2 className="text-5xl font-bold mt-8">DELUXE ROOM</h2>
          <div className="relative w-full h-96 mt-8">
            <Image
              src="/assets/photos/room_1.jpg"
              alt="Deluxe Room"
              layout="fill"
              objectFit="cover"
            />
          </div>

          <button className="bg-red-500 text-white text-xl py-2 px-4 rounded mt-8">
            Book
          </button>

          <div className="flex justify-center mt-4 space-x-4">
            <img
              src="/assets/icons/visa.svg"
              alt="Visa"
              className="w-12 h-12"
            />
            <img
              src="/assets/icons/mastercard.svg"
              alt="MasterCard"
              className="w-12 h-12"
            />
            <img
              src="/assets/icons/amex.svg"
              alt="American Express"
              className="w-12 h-12"
            />
            <img
              src="/assets/icons/paypal.svg"
              alt="PayPal"
              className="w-12 h-12"
            />
          </div>
        </main>
      </div>
    </section>
  );
};

export default RoomDetails;
