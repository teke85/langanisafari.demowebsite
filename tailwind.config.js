/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: ['class'],
  content: [
    './pages/**/*.{js,jsx}',
    './components/**/*.{js,jsx}',
    './app/**/*.{js,jsx}',
    './src/**/*.{js,jsx}',
  ],
  prefix: '',
  theme: {
    container: {
      center: true,
      padding: '2rem',
      screens: {
        '2xl': '1400px',
      },
    },
    extend: {
      keyframes: {
        scroll: {
          '0%': { transform: 'translateX(0)' },
          '100%': { transform: 'translateX(-50%)' },
        },
        'accordion-down': {
          from: { height: '0' },
          to: { height: 'var(--radix-accordion-content-height)' },
        },
        'accordion-up': {
          from: { height: 'var(--radix-accordion-content-height)' },
          to: { height: '0' },
        },
      },
      animation: {
        scroll: 'scroll 45s linear infinite',
        'accordion-down': 'accordion-down 0.2s ease-out',
        'accordion-up': 'accordion-up 0.2s ease-out',
      },
      colors: {
        softwhite: '#eaeaea',
        softblack: '#0e0e0e',
        softgray: '#999D9E',
      },
      fontFamily: {
        lora: ['var(--font-lora)'],
        poppins: ['var(--font-poppins)'],
        ubuntu: ['var(--font-ubuntu)'],
        montserrat: ['var(--font-montserrat)'],
        oswald: ['var(--font-oswald)'],
        enchants: ['var(--font-ENCHANTS)'],
        ibarra: ['var(--font-ibarra)'],
        aboreto: ['var(--font-aboreto)'],
        Roashe: ['var(--font-Roashe'],
      },
    },
  },
  plugins: [require('tailwindcss-animate')],
};
